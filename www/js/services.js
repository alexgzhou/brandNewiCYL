angular.module('icyl.services', ['ngResource'])

.constant('USER_ROLES', {
  all: '*',
  admin: 'admin',
  editor: 'editor',
  user: 'user',
  guest: 'guest'
})

// 栏目大类
.constant('TABS', {
  simpleHomepage: '智慧团青',
  simpleArticleList: '时事资讯',
  simpleActivityList: '活动超市',
  simpleServiceList: '服务列表'
})
// 栏目名称
.constant('TAB_NAME', {
  '299': '党建动态',
  '300': '通知公告',
  '301': '团讯要闻',
  '302': '头条新闻',
  '304': '书记寄语',
  '303': '案例共享',
  '306': '微客大咖',
  '318': '扶持政策',
  '320': '创业项目',
  '319': '招聘信息',
  'pbhd001': '评比活动',
  '308': '部门活动',
  'zbhd001': '支部活动',
  '310': '活动回顾'
})

//本地存储函数
.factory('Storage', ['$window', function($window) {
  return {
    kset: function(key, value) {
      $window.localStorage.setItem(key, value);
    },
    kget: function(key) {
      return $window.localStorage.getItem(key);
    },
    kremove: function(key) {
      $window.localStorage.removeItem(key);
    },
    kclear: function() {
      $window.localStorage.clear();
    }
  };
}])

//非持久化session函数
.service('Session', function () {
  this.isAuthenticated = false;
  this.userRole = 0;
  this.create = function (token) {
    this.token = token;
  };
  this.destroy = function () {
    this.token = null;
  };
  return this;
})

// //offline.js校验函数封装
// .factory('Offline', ['$window', '$q', function($window, $q) {
//   return {
//     offlineCheck: function() {
//       // var deferred = $q.defer();

//       // myAsyncCall().success(function(data) {
//       //   deferred.resolve(data);
//       // });
//       // return deferred.promise;
//     }
//   };
// }])

//数据模型函数
.factory('Data', ['$resource', function ($resource) {
  var baseUrl = 'http://app.go5le.net/';
  return {
    // User: $resource('http://17f.go5le.net/mall/index/chklogin_app.asp', 
    // User: $resource('http://:baseurl/:path/public/:route',
    User: $resource(baseUrl + ':route', 
                    {
                      // baseurl:'localhost', 
                      // path:'myserver',
                      callback: 'JSON_CALLBACK' //jsonp_flag
                    }, 
                    {
                      //signin: {method:'POST', params:{c:'user', a:'get_token'}, timeout: 3000},//json_flag
                      //signup: {method:'POST', params:{c:'user', a:'register'}, timeout: 3000}, //json_flag
                      //checktoken: {method:'POST', params:{c:'user', a:'user_verify'}, timeout: 3000}, //json_flag
                      //updateuserinfo: {method:'POST', params:{c:'user', a:'update_userinfo'}, timeout: 3000},  //json_flag
                      // signin: {method:'JSONP', params:{route:'login'}, timeout: 3000}, //jsonp_flag
                      signup: {method:'JSONP', params:{c:'user', a:'register'}, timeout: 3000},  //jsonp_flag
                      checktoken: {method:'JSONP', params:{c:'user', a:'user_verify'}, timeout: 3000}, //jsonp_flag
                      // updateuserinfo: {method:'JSONP', params:{c:'user', a:'update_userinfo'}, timeout: 3000},  //jsonp_flag
                      signin: {method:'POST', params:{route:'login'}, timeout: 3000}, //json_flag laravel
                      // signup: {method:'POST', params:{route:'register'}, timeout: 3000},  //json_flag laravel
                      // checktoken: {method:'POST', params:{route:'checkToken'}, timeout: 3000}, //json_flag laravel
                      updateuserinfo: {method:'POST', params:{route:'updateUserinfo'}, timeout: 3000},  //json_flag laravel
                      update_avatar: {method:'POST'},
                      update_mobile: {method:'POST'},
                      update_password: {method:'POST'}
                    }),
    Post: $resource('http://17f.go5le.net/mall/index/app_news12.asp',
                    {
                      // baseurl:'localhost',
                      // path:'good'
                      callback: 'JSON_CALLBACK' //jsonp_flag
                    },
                    {
                      loadarticle: {method:'JSONP', timeout: 3000},
                      loadcomments: {method:'JSONP', timeout: 3000},
                      submitcomment: {method:'JSONP', timeout: 3000}
                    }),
    articleList: $resource('http://17f.go5le.net/mall/index/app_news.asp',
                    {
                      // baseurl:'localhost',
                      // path:'good'},
                      callback: 'JSON_CALLBACK' //jsonp_flag
                    },
                    {
                      loadlist: {method:'JSONP', timeout: 3000}
                    }),
    activityList: $resource('http://17f.go5le.net/mall/index/app_hd.asp',
                    {
                      // baseurl:'localhost',
                      // path:'good'},
                      callback: 'JSON_CALLBACK' //jsonp_flag
                    },
                    {
                      loadlist: {method:'JSONP', timeout: 3000}
                    }),
    Comments: $resource('http://17f.go5le.net/mall/index/app_pj.asp',
                    {
                      // baseurl:'localhost',
                      // path:'good'},
                      callback: 'JSON_CALLBACK' //jsonp_flag
                    },
                    {
                      loadcomments: {method:'JSONP', timeout: 3000}
                    }),
    Comment: $resource('http://17f.go5le.net/99_tj/991/news1_app.asp',
                    {
                      // baseurl:'localhost',
                      // path:'good'
                      callback: 'JSON_CALLBACK' //jsonp_flag
                    },
                    {
                      submitcomment: {method:'JSONP', timeout: 3000}
                    }),
  };
}])

//用户操作函数
.factory('User', [ '$ionicModal', 'Storage', 'Data', '$state', 'Session', '$q', '$window', '$ionicPopup', '$ionicHistory', function ($ionicModal, Storage, Data, $state, Session, $q, $window, $ionicPopup, $ionicHistory) {
  return {
    userLogin: function($scope) {
      $scope.loginData = {
        username: '91',
        password: '123',
        rememberPwd: true
      };

      //console.log("#18----------"+$scope.$id);  //=====================test
      // Create the login modal that we will use later
      $ionicModal.fromTemplateUrl('templates/common/login.html', {
        scope: $scope,
        animation: 'slide-in-up'
        //,animation: 'no-animation'
      }).then(function(modal) {
        $scope.loginmodal = modal;
        $scope.loginmodal.show(); //20140804: 直接在这里打开登录窗口，因为是异步加载，所以在其他地方马上打开会因为模板还没加载完成而出错
        //console.log("#17----------"+$scope.$id);  //=====================test
      });

      // Triggered in the login modal to close it
      $scope.actions.closeLogin = function() {
        $scope.loginmodal.hide();
      };

      // Open the login modal
      $scope.actions.login = function() {
        $scope.loginmodal.show();
        //console.log("#login----------"+$scope.$id);  //=====================test
      };

      $scope.actions.preRegister = function() {
        $scope.actions.closeLogin();
        $scope.actions.register();
      };

      // Perform the login action when the user submits the login form
      $scope.actions.doLogin = function() {
        console.log('正在登录', $scope.loginData);

        Data.User.signin($scope.loginData, function(data) {
          // console.log(data.data.token);

          if (data.err_code === 0) { 
            //Alert(data.data.user + ' 您好，欢迎回来！' ); 
            $scope.loginmodal.remove();
            // $ionicModal.fromTemplateUrl('templates/common/login.html', {
            //  scope: $scope
            // }).then(function(modal) {
            //  $scope.loginmodal = modal;
            // });
            if ($scope.loginData.rememberPwd === true) {
              Storage.kset('password', $scope.loginData.password);
            }
            Storage.kset('username', data.data.username);

            // // chklogin_app.asp里面已经设置cookie了，不需要再设置了，可以完全抛弃postMessage.asp的方法以及directive中的iframeSetCookie 
            // =====================================================================
            // // 用postMessage.asp设置cookie=============方法不好，以后会弃用
            // var cookie = "xsunion=no1="+data.data.username+"&pwd="+data.data.password;
            // Storage.kset('xsunion', cookie);
            // var setcookieiframe = document.querySelector('#setcookie iframe');
            // console.log(setcookieiframe);
            // console.log(setcookieiframe.contentWindow);
            // setcookieiframe.contentWindow.postMessage(cookie, 'http://17f.go5le.net');
            // // =====================================================================

            // Storage.kset('token', data.data.token);  //########################key, use in the future
            Session.create(data.data.token);  //########################key, use in the future
            //$scope.mine.mineNgclick = '';
            //$scope.mine.minehref = '#/main/mine';
            //Alert(data.data.token+'=='+data.data.username+'=='+data.data.password+'=='+$scope.loginData.rememberPwd);
            $scope.loginData.password = '';
            if ($scope.actions.toState.access.menuToggle) {
              $ionicHistory.nextViewOptions({
                disableBack: true
              });
            }
            //$scope.loginData.rememberPwd = false;
            //!!$scope.actions.toState ? $state.go($scope.actions.toState) : $window.location.reload(); //$state.reload()方法还有bug，不能重新实例化controller
            //!!$scope.actions.toState ? $state.go($scope.actions.toState) : $state.transitionTo($state.current, $stateParams, { reload: true, inherit: false, notify: true }); //替代方法1，$state.reload()方法还有bug，不能重新实例化controller
            !!$scope.actions.toState ? $state.go($scope.actions.toState) : $state.go('.', {}, {reload: true}); //替代方法2，$state.reload()方法还有bug，不能重新实例化controller
            //!!$scope.actions.toState ? $state.go($scope.actions.toState) : $state.go($state.current, {}, {reload: true}); //替代方法3，$state.reload()方法还有bug，不能重新实例化controller
            //$state.go('main.mine'); //===================使用$state.go跳转到main.mine页面
            //console.log("#16----------"+$state.current.name);  //=====================test
          }
          else {
            $ionicPopup.alert({template: data.err_code + '：' + data.err_msg});
            $scope.loginData.password = '';
          }
        }, function(err){
          $ionicPopup.alert({template: '请检查网络！'});
          console.log(' request fail for login !!!!! ' + err);
        });
      };

      //$scope.actions.login();
    },

    userRegister: function($scope) {
      $scope.registerData = {
        // username: 'alexgzhou',
        // password: '123456789',
        // repeatpassword: '123456789',
        // name: '周天舒',
        // mobile: '13282037883',
        gender: false
      };

      //Alert(registerData.gender);

      // Create the login modal that we will use later
      $ionicModal.fromTemplateUrl('templates/common/register.html', {
        scope: $scope,
        animation: 'slide-in-up'
        //,animation: 'no-animation'
      }).then(function(modal) {
        $scope.registermodal = modal;
      });

      // Triggered in the login modal to close it
      $scope.actions.closeRegister = function() {
        $scope.registermodal.hide();
      };

      // Open the login modal
      $scope.actions.register = function() {
        $scope.registermodal.show();
      };

      $scope.actions.prelogin = function() {
        $scope.actions.closeRegister();
        $scope.actions.login();
      };

      // Perform the register action when the user submits the register form
      $scope.actions.doRegister = function() {
        //$scope.registerData.gender = $scope.registerData.genderFlag ? '女': '男';

        console.log('正在注册', $scope.registerData);

        Data.User.signup($scope.registerData, function(data) {
          //Alert($scope.registerData.genderFlag);
          
          if (data.err_code === 0) { 
            //Alert(data.data.user + ' 注册成功，用户名：' + data.data.username ); 
            $scope.registermodal.remove();
            $ionicModal.fromTemplateUrl('templates/common/register.html', {
             scope: $scope
            }).then(function(modal) {
             $scope.registermodal = modal;
            });
            $scope.loginData = {
              username: $scope.registerData.username,
              rememberPwd: true
            };
            $scope.registerData = {
              gender: false
            };
            $scope.actions.login(); 
          }
          else {
            $ionicPopup.alert({template: data.err_code + '：' + data.err_msg});
            $scope.registerData.password = '';
            $scope.registerData.repeatpassword = '';
            // $scope.registerData = {
            //   gender: false
            // };
          }
        }, function(err){
          $ionicPopup.alert({template: '请检查网络！！'});
          console.log(' request fail for register !!!!! ' + err);
        });
      };
    },

    userLogout: function() {
      Session.destroy();
      Storage.kremove('password');
      // Storage.kremove('token');  //########################key, use in the future
      $ionicHistory.nextViewOptions({
        disableBack: true,
        historyRoot: true
      });
      $state.go('simple.homepage');
    },

    getUserInfo: function() {
      var deferred = $q.defer();
      Data.User.checktoken({token: Session.token}, function(data) {
        deferred.resolve(data);
        //console.log('#2--------------'+Session.token);
      }, function(err) {
        $ionicPopup.alert({template: '请检查网络！！！'});
        console.log(' request fail for getUserInfo !!!!! ' + err);
        deferred.resolve(err);
      });
      return deferred.promise;
    },

    updateUserInfo: function(userinfo) {
      var deferred = $q.defer();
      userinfo.token = Session.token;
      //console.log(userinfo);
      Data.User.updateuserinfo(userinfo, function(data) {
        deferred.resolve(data);
      }, function(err) {
          $ionicPopup.alert({template: '请检查网络！！！！'});
          console.log(' request fail for updateUserInfo !!!!! ' + err);
          deferred.resolve(err);
      });
      return deferred.promise;
    }
  };
}])

//安全认证函数
.factory('Identification', ['Storage', 'Data', 'Session', '$q', '$ionicPopup', function (Storage, Data, Session, $q, $ionicPopup) {
  return {
    checkToken: function() {
      //$scope.mine = {};
      //$scope.mine.mineNgclick = "actions.login()";
      //console.log("#4----------"+$scope.$id);  //=====================test
      // console.log("checkToken入口: " + Session.token);
      //设置promise
      var deferred = $q.defer();
      //checkToken具体步骤
      if (Storage.kget('username') && Storage.kget('password')) {
        if (Session.token) {
          Data.User.checktoken({token: Session.token}, function(data) {
            if (data.err_code === 0) { 
              //Actions.mineClick.allowed($scope);
              //console.log("#5----------"+$scope.$id);  //=====================test
              //return true;
              // console.log(data.data.token);
              deferred.resolve(data);
            }
            else {
              Data.User.signin({username: Storage.kget('username'), password: Storage.kget('password')}, function(data) {
                if (data.err_code === 0) { 
                  // Storage.kset('token', data.data.token);  //########################key, use in the future
                  // Session.create(data.data.token);  //########################key, use in the future
                  Session.create(data.data.token ? data.data.token : "whatever");  //########################Temp, remove in the future
                  //Actions.mineClick.allowed($scope);
                  //console.log("#6----------"+$scope.$id);  //=====================test
                  //return true;
                  // console.log(data.data.token);
                  deferred.resolve(data);
                }
                else {
                  //Actions.mineClick.denied($scope);
                  //console.log("#7----------"+$scope.$id);  //=====================test
                  //return false;
                  deferred.resolve(data);
                }
              }, function(err) {
                $ionicPopup.alert({template: '请检查网络！！！！！'});
                console.log(' request fail for get_token !!!!! ' + err);
                deferred.resolve(err);
              });
            }
          }, function(err) {
              $ionicPopup.alert({template: '请检查网络！！！！！！'});
              console.log(' request fail for check_token !!!!! ' + err);
              deferred.resolve(err);
          });
        }
        else {
          Data.User.signin({username: Storage.kget('username'), password: Storage.kget('password')}, function(data) {
            if (data.err_code === 0) { 
                // Storage.kset('token', data.data.token);  //########################key, use in the future
                // Session.create(data.data.token);  //########################key, use in the future
                Session.create(data.data.token ? data.data.token : "whatever");  //########################Temp, remove in the future
                //Actions.mineClick.allowed($scope);
                //console.log("#8----------"+$scope.$id);  //=====================test
                //return true;
                // console.log("Session.create: " + data.data.token);
                deferred.resolve(data);
              }
              else {
                //Actions.mineClick.denied($scope);
                //console.log("#9----------");  //=====================test
                //return false;
                deferred.resolve(data);
              }
            }, function(err) {
              $ionicPopup.alert({template: '请检查网络！！！！！！！'});
              console.log(' request fail for get_token !!!!!!! ' + err);
              deferred.resolve(err);
            });
        }
      }
      else {
        if (Session.token) {
          // console.log("#10----------"+$scope.$id);  //=====================test
          Data.User.checktoken({token: Session.token}, function(data) {
            if (data.err_code === 0) { 
              //Actions.mineClick.allowed($scope);
              //console.log("#11----------"+$scope.$id);  //=====================test
              // return true;
              deferred.resolve(data);
            }
            else {
              //console.log("#12----------"+$scope.$id);  //=====================test
              //Actions.mineClick.denied($scope);
              // return false;
              deferred.resolve(data);
            }
          }, function(err) {
              $ionicPopup.alert({template: '请检查网络！！！！！！！！'});
              console.log(' request fail for check_token without username and password !!!!! ' + err);
              deferred.resolve(err);
          });
        }
        else {
          //Actions.mineClick.denied($scope);
          //console.log("#13----------"+Session.token);  //=====================test
          // return false;
          var data = {};
          data.err_code = -999;
          deferred.resolve(data);
        }
      }
      return deferred.promise;
    },

    isAuthenticated: function () {
      return !!Session.token;
    }
  };
}])

// 页面功能封装
.factory('PageFunctions', ['Data', '$stateParams', '$ionicNavBarDelegate', 'TABS', 'TAB_NAME', '$timeout', '$rootScope', '$ionicPopup', '$ionicScrollDelegate', '$ionicHistory', function (Data, $stateParams, $ionicNavBarDelegate, TABS, TAB_NAME, $timeout, $rootScope, $ionicPopup, $ionicScrollDelegate, $ionicHistory) {
  return {
    listFunc: function ($scope, controller, resType, resMethod) {
      // var localData;
      var listType = $stateParams.tabCode && $stateParams.tabCode || '';
      var pageParams = {
        tabCode: listType,
        loaded: 0,
        lastID: 0,
        requestNO: 20
      };
      $scope.items = [];
      var moreData = false;

      var myDate = new Date();
      var lastTime; // = myDate.getTime();
      // console.log(lastTime);

      var setItems = function (itemArray) {
        $scope.items = itemArray;
        pageParams.loaded = $scope.items.length;
        pageParams.lastID = $scope.items[$scope.items.length - 1] && $scope.items[$scope.items.length - 1][0] || 0;
        moreData = true;
      };

      // $scope.init = function () {  // 这里不能定义为$scope.init = function(){};，$scope.init必须在.html中调用，在这里(.js中)调用会出现奇怪的问题。
      var init = function () {
        // console.log(pageParams);
        // console.log(resType, resMethod, $rootScope.myOnline, Connection.NONE);
        // $scope.refreshable = false;
        // $scope.loadable = false;

        if ($rootScope.myOnline !== Connection.NONE) {
          // if (!lastTime || parseInt((myDate.getTime() - lastTime)/10000) > 1) {
            Data[resType][resMethod](pageParams, function (data) {
              setItems(data.data.items);

              // localData = JSON.stringify($scope.items);
              // myDate = new Date();
              lastTime = myDate.getTime();
              // console.log(data.data.items);
              // Storage.kset(controller + '_' + listType + '_time', myDate.getTime());
              // Storage.kset(controller + '_' + listType + '_data', localData);

              // console.log('moreData:', moreData);
            }, function (err) {
              // // $ionicPopup.alert({template: '当前网络不佳！' + $rootScope.myOnline});
              // localData = JSON.parse(Storage.kget(controller + '_' + listType + '_data'));
              // if (!!localData) {
              //   setItems(localData); 
              // }
              $scope.refreshable = true;
              $scope.refreshableTitle = '网络不佳，请点击或下拉重新加载！';
              console.log(err);
            });  
          // }
          // else {
          //   localData = JSON.parse(Storage.kget(controller + '_' + listType + '_data'));
          //   if (!!localData) {
          //     setItems(localData); 
          //   }
          //   else {
          //     $scope.refreshable = true;
          //     $scope.refreshableTitle = '未知问题，请点击或下拉重新加载！';
          //   }
          // }  
        }
        else {
          // localData = JSON.parse(Storage.kget(controller + '_' + listType + '_data'));
          // if (!!localData) {
          //   setItems(localData); 
          // }
          $scope.refreshable = true;
          $scope.refreshableTitle = '当前离线，请点击或下拉重新加载！';
        }
      };

      // // $scope.init(); //会出现下拉无法加载的情况，原因分析：$scope.init()必须在.html模板中被调用，不能直接在.js程序里面被调用，不然会出现上述奇怪的问题，可能因为带$scope的函数作用域是.html中的DOM。
      init();

      // 下拉刷新
      $scope.doRefresh = function () {
        if ($rootScope.myOnline !== Connection.NONE) {
          pageParams.lastID = 0;
          pageParams.requestNO = pageParams.loaded > 100 ? 100 : pageParams.loaded;
          pageParams.requestNO = pageParams.loaded ? pageParams.loaded : 20;
          // console.log(pageParams);
          // console.log(resType, resMethod, $rootScope.myOnline, Connection.NONE);
          Data[resType][resMethod](pageParams, function (data) {
            setItems(data.data.items);

            // localData = JSON.stringify($scope.items);
            myDate = new Date();
            lastTime = myDate.getTime();
            // Storage.kset(controller + '_' + listType + '_time', myDate.getTime());
            // Storage.kset(controller + '_' + listType + '_data', localData);

            $scope.$broadcast('scroll.refreshComplete');
            pageParams.requestNO = 20;
            $scope.refreshable = false;
            $scope.loadable = false;
            // console.log($scope.items);
          }, function (err) {
            // localData = JSON.parse(Storage.kget(controller + '_' + listType + '_data'));
            // if (!!localData) {
            //   setItems(localData); 
            // }
            $scope.$broadcast('scroll.refreshComplete');
            // $ionicPopup.alert({template: '当前网络不佳，请重新下拉刷新！'});
            $scope.refreshable = true;
            $scope.refreshableTitle = '当前网络不佳，请点击或下拉刷新！';
            // console.log(err);
          });
        }
        else {
          // localData = JSON.parse(Storage.kget(controller + '_' + listType + '_data'));
          // if (!!localData) {
          //   setItems(localData); 
          // }
          $scope.$broadcast('scroll.refreshComplete');
          // $ionicPopup.alert({template: '当前没有网络连接，不能刷新！'});
          $scope.refreshable = true;
          $scope.refreshableTitle = '当前没有网络连接，不能刷新！';
          // console.log($rootScope.myOnline);
        }
      };

      //上拉加载
      $scope.loadMoreData = function () {
        if ($rootScope.myOnline !== Connection.NONE) {
          // console.log(pageParams);
          // console.log(resType, resMethod, $rootScope.myOnline, Connection.NONE);
          Data[resType][resMethod](pageParams, function (data) {
            $scope.items = $scope.items.concat(data.data.items);
            // Storage.kset(pageParams[index].tabCode, $scope.itemss[index].length);
            pageParams.loaded = $scope.items.length;
            pageParams.lastID = $scope.items[$scope.items.length - 1] && $scope.items[$scope.items.length - 1][0] || 0;

            // localData = JSON.stringify($scope.items);
            // Storage.kset(controller + '_' + listType + '_data', localData);

            Data[resType][resMethod](pageParams, function (data) {
              if (data.data.items.length < 1) {
                moreData = false;
                // Storage.kset('moreData', moreData);
                // console.log("没有更多内容啦！");
              }
              // console.log(moreData);
            }, function (err) {
              // $ionicPopup.alert({template: '当前网络不佳，未知剩余条数！'});
              $scope.loadable = true;
              $scope.loadableTitle = '当前网络不佳，未知剩余条数！';
              // console.log(err);
            });

            $scope.$broadcast('scroll.infiniteScrollComplete');
            $scope.refreshable = false;
            $scope.loadable = false;
            // console.log('loadMoreData:', moreData);
          }, function (err) {
            // // $rootScope.myOnline = Connection.NONE;
            // localData = JSON.parse(Storage.kget(controller + '_' + listType + '_data'));
            // if (!!localData) {
            //   setItems(localData); 
            // }
            $scope.$broadcast('scroll.infiniteScrollComplete');
            // $ionicPopup.alert({template: '当前网络不佳，请重新加载！'});
            $scope.loadable = true;
            $scope.loadableTitle = '当前网络不佳，请重新加载！';
            // console.log(err);
          });
        }
        else {
          // localData = JSON.parse(Storage.kget(controller + '_' + listType + '_data'));
          // if (!!localData) {
          //   setItems(localData); 
          // }
          $scope.$broadcast('scroll.infiniteScrollComplete');
          // $ionicPopup.alert({template: '当前没有网络连接，不能加载！'});
          $scope.loadable = true;
          $scope.loadableTitle = '当前没有网络连接，不能加载！';
          // console.log($rootScope.myOnline);
        }
      };
      $scope.moreDataCanBeLoaded = function () {
        // var isOnline;
        // if($rootScope.myOnline !== Connection.NONE && $rootScope.myOnline !== Connection.UNKNOWN) {
        //   isOnline = true;
        // }
        // return moreData && isOnline && !$scope.loadable;
        return moreData && !$scope.loadable;
      };
      $scope.$on('stateChangeSuccess', function () {
        $scope.loadMoreData();
      });
      $scope.refreshAgain = function () {
        $scope.doRefresh();
      };
      $scope.loadMore = function () {
        $scope.loadable = false;
        $ionicScrollDelegate.scrollBottom(true);
      };

      var onOnline = $scope.$on('onOnline', function () {
        // moreData = true;
        $scope.$apply(function () {
          $scope.refreshable = false;
          $scope.loadable = false;
        });

        // $ionicPopup.alert({title: '在线事件', template: $rootScope.myOnline, okText: '确认'});
        $ionicNavBarDelegate.title(TAB_NAME[listType] ? TAB_NAME[listType] : TABS[controller]);

        // console.log($scope.items);
        // $scope.items = [];
        if ($scope.items.length < 1) {
          // console.log(pageParams);
          // console.log(resType, resMethod, $rootScope.myOnline, Connection.NONE, $scope.items.length);
          // $scope.doRefresh();  //这里不能调用$scope.doRefresh()，$scope.items数据都有了，但是会出现列表不显示的问题，原因同上，原因分析：$scope.init()必须在.html模板中被调用，不能直接在.js程序里面被调用，不然会出现上述奇怪的问题，可能因为带$scope的函数作用域是.html中的DOM。
          pageParams = {
            tabCode: listType,
            loaded: 0,
            lastID: 0,
            requestNO: 20
          };
          init();
          // console.log(pageParams);
        }
      });
      var onOffline = $scope.$on('onOffline', function () {
        // $ionicPopup.alert({title: '离线事件', template: $rootScope.myOnline, okText: '确认'});
        // var offTitle = $ionicNavBarDelegate.getTitle() + "(离线)";
        var offTitle = "当前离线";
        $ionicNavBarDelegate.title(offTitle);
        $scope.$broadcast('scroll.refreshComplete');
        $scope.$broadcast('scroll.infiniteScrollComplete');
        // console.log(moreData, $scope.loadable);
      });

      // // TAB_NAME[listType] ? console.log(TAB_NAME[listType]) : console.log(TABS[controller]);
      // $timeout(function () {
      //   $ionicNavBarDelegate.title(TAB_NAME[listType] ? TAB_NAME[listType] : TABS[controller]);
      // });
      // console.log('controller:', controller, '; listType:', listType);
      $scope.$on('$ionicView.beforeEnter', function() {
        // console.log(TABS[controller], TAB_NAME[listType]);
        $timeout(function () {
          $ionicNavBarDelegate.title(TAB_NAME[listType] ? TAB_NAME[listType] : TABS[controller]);
          // $ionicHistory.currentTitle(TAB_NAME[listType] ? TAB_NAME[listType] : TABS[controller]);
          // console.log(TABS[controller], '---TABS_CONSTANT---', TAB_NAME[listType]);
          // console.log($ionicNavBarDelegate.title());
          // console.log($ionicHistory.currentTitle(), '---$ionicHistory---', $ionicHistory.backTitle());
        });

        // lastTime = Storage.kget(controller + '_' + listType + '_time');
        myDate = new Date();
        // console.log(myDate.getTime(), '-', lastTime, '=', myDate.getTime() - lastTime);
        if (parseInt((myDate.getTime() - lastTime)/3600000) > 1) {
          // console.log(myDate.getTime() + '-' + lastTime + '=' + (myDate.getTime() - lastTime));
          pageParams.lastID = 0;
          pageParams.loaded = 0;
          pageParams.requestNO = 20;
          moreData = false;
          init();
        }
        
      });
    }
  };
}])


.factory('AuthenticationService', function() {
    var auth = {
        isAuthenticated: false,
        isAdmin: false
    }

    return auth;
})

.factory('TokenInterceptor', function ($q, $window, $location, Session) {
    return {
        request: function (config) {
            config.headers = config.headers || {};
            if ($window.sessionStorage.token) {
                config.headers.Authorization = 'Bearer ' + $window.sessionStorage.token;
            }
            return config;
        },

        requestError: function(rejection) {
            return $q.reject(rejection);
        },

        /* Set Authentication.isAuthenticated to true if 200 received */
        response: function (response) {
            if (response != null && response.status == 200 && $window.sessionStorage.token && !Session.isAuthenticated) {
                Session.isAuthenticated = true;
            }
            return response || $q.when(response);
        },

        /* Revoke client authentication if 401 is received */
        responseError: function(rejection) {
            if (rejection != null && rejection.status === 401 && ($window.sessionStorage.token || Session.isAuthenticated)) {
                delete $window.sessionStorage.token;
                Session.isAuthenticated = false;
                $location.path("/admin/login");
            }

            return $q.reject(rejection);
        }
    };
})
;
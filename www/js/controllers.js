angular.module('icyl.controllers', [])

.controller('index', ['$scope', function ($scope) {
}])

.controller('initSplashes', ['$scope', 'Storage', function ($scope, Storage) {
  // Storage.kset('initState', 'simple.homepage');
  Storage.kset('myAppVersion', myAppVersion);
}])

.controller('simpleContainer', ['$scope', function ($scope) {
}])

// 首页
.controller('simpleHomepage', ['$scope', 'PageFunctions', function ($scope, PageFunctions) {
  PageFunctions.listFunc($scope, 'simpleHomepage', 'articleList', 'loadlist');
  // $scope.$on('$ionicView.enter', function() {
  //   console.log($scope.items);
  // });
  // console.log('simpleHomepage');
}])

// 我的主页
.controller('simpleMine', ['$scope', function ($scope) {
  $scope.userID = '1';
  $scope.userName = '王成';
}])

// 文章导航
.controller('simpleNavArticle', ['$scope', '$ionicHistory', '$ionicNavBarDelegate', '$timeout', function ($scope, $ionicHistory, $ionicNavBarDelegate, $timeout) {
  // $scope.$on('$ionicView.beforeEnter', function() {
  //   // console.log(TABS[controller], TAB_NAME[listType]);
  //   $timeout(function () {
  //     $ionicNavBarDelegate.title('时事资讯');
  //   });
  // });
  // $scope.changeTitle = function () {
  //   // console.log($ionicHistory.viewHistory());
  //   // console.log($ionicHistory.currentView());
  //   // console.log($ionicHistory.currentHistoryId());
  //   // console.log($ionicHistory.currentTitle());
  //   // console.log($ionicHistory.backView());
  //   // console.log($ionicHistory.backTitle());
  //   // console.log($ionicHistory.forwardView());
  //   // console.log($ionicHistory.currentStateName());
  //   $scope.$apply(function () {
  //     $ionicHistory.currentTitle('hahahaha');
  //   });
  // };
}])

// 活动导航
.controller('simpleNavActivity', ['$scope', function ($scope) {
}])

// 服务导航
.controller('simpleNavService', ['$scope', function ($scope) {
}])

// 文章列表
.controller('simpleArticleList', ['$scope', 'PageFunctions', function ($scope, PageFunctions) {
  PageFunctions.listFunc($scope, 'simpleArticleList', 'articleList', 'loadlist');
  // console.log('simpleArticleList');
}])

// 活动列表
.controller('simpleActivityList', ['$scope', 'PageFunctions', 'Storage', function ($scope, PageFunctions, Storage) {
  PageFunctions.listFunc($scope, 'simpleActivityList', 'activityList', 'loadlist');
  // console.log('simpleActivityList');
  $scope.username = Storage.kget('username');
}])

// 服务列表
.controller('simpleServiceList', ['$scope', 'PageFunctions', '$ionicModal', function ($scope, PageFunctions, $ionicModal) {
  PageFunctions.listFunc($scope, 'simpleServiceList', 'activityList', 'loadlist');
  // console.log('simpleServiceList');
  
  $scope.getItemHeight = function (item, index) {
    //Make evenly indexed items be 10px taller, for the sake of example
    return (index % 2) === 0 ? 150 : 160;
  };

  $scope.priceRegions = [
    {'priceRegion': "0-100"},
    {'priceRegion': '100-200'},
    {'priceRegion': '200-400'},
  ];

  $scope.priceRegion = '200-400';

  $scope.categoryList = [
    {'categoryName': '服饰/鞋/包/配饰'},
    {'categoryName': '居家生活'},
    {'categoryName': '母婴童装'},
    {'categoryName': '手机数码'},
    {'categoryName': '家用电器'}
  ];

  $scope.category = '手机数码';

  $scope.regions = [
    {'region': '浙江省'},
    {'region': '杭州市'},
    {'region': '台州市'},
    {'region': '宁波市'},
    {'region': '绍兴市'}
  ];

  $scope.items = [
    {'name': '奥普兰沙发',
     'provider': '奥普兰',
     'price': '1000',
     'region': 'A区',
     'commentNum':'2',
     'category':'家居生活',
     'imageUrl': "img/main_backimg_fade.png"
    },
    {'name': '奥普兰床',
     'provider': '奥普兰',
     'price': '1000',
     'region': 'A区',
     'commentNum':'2',
     'category':'家居生活',
     'imageUrl': "img/main_backimg_fade.png"
    },
    {'name': '奥普兰餐桌',
     'provider': '奥普兰',
     'price': '1000',
     'region': 'A区',
     'commentNum':'2',
     'category':'家居生活',
     'imageUrl': "img/main_backimg_fade.png"
   }
  ];

  $ionicModal.fromTemplateUrl('commodityModal.html', {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function (modal) {
    $scope.commodityModal = modal;
  });
  $scope.openModal = function () {
    $scope.commodityModal.show();
  };
  $scope.closeModal = function () {
    $scope.commodityModal.hide();
  };
  //Cleanup the modal when we're done with it!
  $scope.$on('$destroy', function () {
    $scope.commodityModal.remove();
  });
  // Execute action on hide modal
  $scope.$on('modal.hidden', function () {
    // Execute action
  });
  // Execute action on remove modal
  $scope.$on('modal.removed', function () {
    // Execute action
  });
}])

// 文章内容及评价
.controller('simpleArticle', ['$scope', '$stateParams', 'Data', 'Identification', "User", "Storage", '$ionicPopup', '$ionicHistory', '$ionicNavBarDelegate', '$timeout', function ($scope, $stateParams, Data, Identification, User, Storage, $ionicPopup, $ionicHistory, $ionicNavBarDelegate, $timeout) {
  $scope.submit = {
    Comment: ''
  }; //必须这样ng-model才是双向的two-way
  var pageParams = 
  {
    articleId: $stateParams.articleId,
    loaded: 0,
    lastID: 0,
    requestNO: 20
  };
  var replyParams = 
  {
    username: Storage.kget('username'),
    // password: Storage.kget('password'),
    content: $scope.submit.Comment,
    id: $stateParams.articleId,
    act: 'dd',
    ztid: '',
    zt: '',
    shopid: ''
  };
  
  $scope.article = {};
  $scope.comments = {};
  var moreData = false;
  
  Data.Post.loadarticle(pageParams, function (data) {
    $scope.article = data.data.items;
    // console.log($scope.article);

    $scope.newsTitle = $scope.article[0][1];
    $scope.imageUrl = $scope.article[0][2];
    $scope.newsDate = $scope.article[0][3];
    $scope.newsSource = $scope.article[0][9];
    $scope.newsContent = $scope.article[0][8];
    // console.log($scope.newsTitle);
    // console.log($scope.newsDate);
    // console.log($scope.newsSource);
    // console.log($scope.newsContent);
    // console.log('loadarticle:' + moreData);
    // moreData = true;
    replyParams.ztid = $scope.article[0][0];
    replyParams.zt = $scope.article[0][1];
    replyParams.shopid = $scope.article[0][7];
    Data.Comments.loadcomments(pageParams, function (data) {
      $scope.comments = data.data.items;
      // console.log('comments:' + moreData);
      moreData = true;
      pageParams.loaded = $scope.comments.length;
      pageParams.lastID = $scope.comments[$scope.comments.length - 1] && $scope.comments[$scope.comments.length - 1][0] || 0;
    });
  });

  $scope.submit = function () {
    // console.log($scope.userComment);
    Identification.checkToken().then(function (data) {
      if (data.err_code === 0) {
        //console.log(data.data); //=====================test
        replyParams.content = $scope.submit.Comment && $scope.submit.Comment || "已阅";
        Data.Comment.submitcomment(replyParams, function (data) {
          $scope.doRefresh();
          $scope.submit.Comment = '';
        });
        // console.log(replyParams);
      }
      else {
        //console.log(data); //=====================test
        $scope.actions = {};
        User.userLogin($scope);
        User.userRegister($scope);
      }
    }, function (err) {
      console.log('错误：Identification.checkToken()' + err);
      $ionicPopup.alert({template: '请检查网络！'});
    });
  };

  //下拉刷新
  $scope.doRefresh = function () {
    pageParams.lastID = 0;
    pageParams.requestNO = pageParams.loaded;
    Data.Post.loadarticle(pageParams, function (data) {
      $scope.article = data.data.items;
      // console.log($scope.article);

      $scope.newsTitle = $scope.article[0][1];
      $scope.imageUrl = $scope.article[0][2];
      $scope.newsDate = $scope.article[0][3];
      $scope.newsSource = $scope.article[0][9];
      $scope.newsContent = $scope.article[0][8];
      // console.log($scope.newsTitle);
      // console.log($scope.newsDate);
      // console.log($scope.newsSource);
      // console.log($scope.newsContent);
      // console.log('loadarticle:' + moreData);
      // moreData = true;
      Data.Comments.loadcomments(pageParams, function (data) {
        $scope.comments = data.data.items;
        // console.log('comments:' + moreData);
        moreData = true;
        $scope.$broadcast('scroll.refreshComplete');
        pageParams.requestNO = 20;
        pageParams.loaded = $scope.comments.length;
        pageParams.lastID = $scope.comments[$scope.comments.length - 1] && $scope.comments[$scope.comments.length - 1][0] || 0;
      });
    }); 
  };

  //上拉加载更多评论：需要在asp端模仿app_news.asp的方式，同样有loaded和lasdID
  $scope.loadMoreData = function () {
    // console.log(moreData);
    Data.Comments.loadcomments(pageParams, function (data) {
      // console.log($scope.comments);
      // if ($scope.comments === {}) {
      //   console.log('$scope.comments');
      //   $scope.comments = data.data.items;
      // }
      // else {
      //   console.log('else');
        $scope.comments.concat(data.data.items);
      // }
      // console.log($scope.comments);
      pageParams.loaded = $scope.comments.length;
      pageParams.lastID = $scope.comments[$scope.comments.length - 1] && $scope.comments[$scope.comments.length - 1][0] || 0;
      $scope.$broadcast('scroll.infiniteScrollComplete');

      Data.Comments.loadcomments(pageParams, function (data) {
        // console.log(data.data.items.length);
        if(data.data.items.length < 1) {
          moreData = false;
        }
        else {
          moreData = true;
        }
      });
    });    
  };
  $scope.moreDataCanBeLoaded = function () {
    return moreData;
  };

  $scope.$on('stateChangeSuccess', function () {
    $scope.loadMoreData();
  });

  // console.log($ionicHistory.backTitle());
  // $timeout(function () {
  //   $ionicNavBarDelegate.title($ionicHistory.backTitle());
  // });
}])

// 活动页面链接
.controller('simpleActivity', ['$scope', '$stateParams', '$sce', function ($scope, $stateParams, $sce) {
  $scope.activityUrl = $sce.trustAsResourceUrl("http://17f.go5le.net/mall/index/pro_show_dj3.asp?id=" + $stateParams.activityId);
}])

// 心理1解1
.controller('simplePsychology', ['$scope', 'Data', '$stateParams', function ($scope, Data, $stateParams) {
  var pageParams = 
  {
  };
  
  $scope.items = [
    ['132074', '记忆能力测试', ''],
    ['132073', '个人能力测试', ''],
    ['132072', '测试你的焦虑程度', '']
  ];
  var moreData = false;
    
  // Data.articleList.loadlist(pageParams, function (data) {
  //   $scope.items = data.data.items;

    moreData = true;
  // });

  //下拉刷新
  $scope.doRefresh = function () {
    
    // Data.articleList.loadlist(pageParams, function (data) {
    //   $scope.articleLists = data.data.items;
    //   pageParams.loaded = $scope.articleLists.length;
    //   pageParams.lastID = $scope.articleLists[$scope.articleLists.length - 1] && $scope.articleLists[$scope.articleLists.length - 1][0] || 0;
      $scope.$broadcast('scroll.refreshComplete');
    // });
    
  };

  //上拉加载
  $scope.loadMoreData = function () {
    // Data.articleList.loadlist(pageParams, function(data){
      // $scope.articleLists = $scope.articleLists.concat(data.data.items);
      // pageParams.loaded = $scope.articleLists.length;
      // pageParams.lastID = $scope.articleLists[$scope.articleLists.length - 1] && $scope.articleLists[$scope.articleLists.length - 1][0] || 0;
      $scope.$broadcast('scroll.infiniteScrollComplete');
    // });

    // Data.articleList.loadlist(pageParams, function (data) {
    //   if(data.data.items.length < 1) {
    //     moreData = false;
    //   }
    //   else {
    //     moreData = true;
    //   }
    // });
    
  };
  $scope.moreDataCanBeLoaded = function () {
    return moreData;
  };

  $scope.$on('stateChangeSuccess', function () {
    $scope.loadMoreData();
  });
}])

// 心理1解1链接
.controller('simplePsychologyLink', ['$scope', '$stateParams', '$sce', function ($scope, $stateParams, $sce) {
  $scope.pUrl = $sce.trustAsResourceUrl("http://17f.go5le.net/99_tj/991/news1_2.asp?id=" + $stateParams.qid);
}])

// 搜索
.controller('simpleSearch', ['$scope', function ($scope) {
  // $scope.item = {
  //   searchContent: 'search'
  // };
}])

// 发布
.controller('simplePublish', ['$scope', function ($scope) {
  $scope.item = {'itemTitle':'',
    'state':'',
    'itemLink':'',
    'contacts':'省直团工委',
    'phone':'13757196484'};

  $scope.types = [
    {name:'通知公告', category:'团讯要闻'},
    {name:'头条新闻', category:'团讯要闻'},
    {name:'新闻', category:'团讯要闻'},
    {name:'本级活动', category:'活动超市'},
    {name:'部门活动', category:'活动超市'},
    {name:'活动回顾', category:'活动超市'},
  ];

  $scope.typeName = $scope.types[0];

  $scope.clearContent = function () {
    $scope.item.itemTitle = '';
    $scope.item.state = '';
    $scope.item.itemLink = '';
  };
}])

// 收藏
.controller('simpleFavorites', ['$scope', '$ionicModal', function ($scope, $ionicModal) {
  $scope.itemList = [
    { text: "通知公告", checked: true },
    { text: "团讯要闻", checked: false },
    { text: "头条新闻", checked: false },
    { text: "书记寄语", checked: false }
  ];

  $ionicModal.fromTemplateUrl('favoritesModal.html', {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function (modal) {
    $scope.favoritesModal = modal;
  });
  $scope.openModal = function () {
    $scope.favoritesModal.show();
  };
  $scope.closeModal = function () {
    $scope.favoritesModal.hide();
  };
  //Cleanup the modal when we're done with it!
  $scope.$on('$destroy', function () {
    $scope.favoritesModal.remove();
  });
  // Execute action on hide modal
  $scope.$on('modal.hidden', function () {
    // Execute action
  });
  // Execute action on remove modal
  $scope.$on('modal.removed', function () {
    // Execute action
  });
}])

// 设置
.controller('simpleSettings', ['$scope', 'Storage', 'User', '$ionicPopup', function ($scope, Storage, User, $ionicPopup) {
  $scope.userName = "王林";
  $scope.signature = "";

  $scope.clearCache = function () {
    var username = Storage.kget('username') && Storage.kget('username') || '';
    var password = Storage.kget('password') && Storage.kget('password') || '';
    var token = Storage.kget('token') && Storage.kget('token') || '';
    var myAppVersionLocal = Storage.kget('myAppVersion') && Storage.kget('myAppVersion') || '';
    // var initState = Storage.kget('initState') && Storage.kget('initState') || '';
    Storage.kclear();
    username ? Storage.kset('username', username) : null;
    password ? Storage.kset('password', password) : null;
    token ? Storage.kset('token', token) : null;
    myAppVersionLocal ? Storage.kset('myAppVersion', myAppVersionLocal) : null;
    // initState ? Storage.kset('initState', initState) : null;
    // ImgCache.clearCache();
    $ionicPopup.alert({template: '清理成功'});
    // navigator.app.clearCache();
  };
  $scope.logout = function () {
    User.userLogout();
  };
}])

// 关于我们
.controller('simpleOurs', ['$scope', function ($scope) {
}])

// 用户协议
.controller('simpleAgreement', ['$scope', function ($scope) {
  $scope.content1 = "";
  $scope.content2 = "";
  $scope.content3 = "";
  $scope.content4 = "";
}])

// 意见反馈
.controller('simpleFeedback', ['$scope', function ($scope) {
}])

// 个人资料
.controller('simplePersonalInfo', ['$scope', function ($scope) {
  var date = new Date('2014-8-15');
  $scope.item = {
    userName: 'wangcheng',
    name: '王成',
    userID: '2014082911423',
    birth: date,
    identityCard: '000000000000000', 

    password: '123',
    repeatPassword: '123',
    phone: '13732255555',
    QQ: '113456789',
    email: '123@qq.com',

    work: '省直机关团工委',
    address: '浙江省杭州市',
    hobby: '读书',
    brief: '无'
  };
}])

// 签到管理
.controller('simplePersonSignIn', ['$scope', function ($scope) {
  $scope.userName = '王成';
  $scope.userID = '2014082911423';
  $scope.currentNum = '0';
  $scope.toBeChecked = '0';
}])

// 个人留言
.controller('simplePersonMessage', ['$scope', function ($scope) {
  $scope.items = [
    {'name': '王成',
     'message': '你好！',
     'date': '2014-8-15'},
    {'name': '王成',
     'message': '你在哪里工作呀？',
     'date': '2014-8-15'},
    {'name': '王林',
     'message': 'HI！',
     'date': '2014-8-15'}
  ];
}])

// 活动列表
.controller('simplePersonManagement', ['$scope', function ($scope) {
  $scope.activityContent = "";

  $scope.types = [
    {name:'全部', category:'全部'},
    {name:'读书会', category:'读书会'},
    {name:'--大型活动', category:'读书会'},
    {name:'--阅读分享', category:'读书会'},
    {name:'团青活动', category:'团青活动'},
    {name:'--相亲活动', category:'团青活动'},
    {name:'--娱体活动', category:'团青活动'},
    {name:'--组织活动', category:'团青活动'},
  ];

  $scope.typeName = $scope.types[0];

  $scope.depts = [
    {name:'全部', category:''},
    {name:'省委办公厅', category:''},
    {name:'浙江省团建', category:''}
  ];

  $scope.deptName = $scope.types[0];

  $scope.items = [
    {activityTitle:'省直机关午间正能量——摄影沙龙活动', 
     typeName:'娱体活动',
     organizer:'浙江省团建',
     number:'0',
     count:'38',
     url:'#/simple/personStatistics'},
    {activityTitle:'关于举办“书海琴缘”省直机关单身青年钢琴训练营的通知', 
     typeName:'娱体活动',
     organizer:'浙江省团建',
     number:'0',
     count:'381',
     url:'#/main/personStatistics'},
    {activityTitle:'真人CS', 
     typeName:'娱体活动',
     organizer:'浙江省团建',
     number:'0',
     count:'328',
     url:'#/main/personStatistics'},
    {activityTitle:'万人相亲会够“疯狂” 解读五宗“最”', 
     typeName:'相亲活动',
     organizer:'浙江省团建',
     number:'0',
     count:'138',
     url:'#/main/personStatistics'},
  ];
}])

// 活动统计
.controller('simplePersonStatistics', ['$scope', function ($scope) {
  $scope.activityTitle = '关于举办“书海琴缘”省直机关单身青年钢琴训练营的通知';
  
  $scope.items = [
    {
      name:'浙商银行', 
      dept:'浙商银行',
      address:'浙江省杭州市',
      phone:'12456789038',
      count:'2',
      isConfirmed:'未确认',
      comment:'0'
    },
    {
      name:'省司法厅', 
      dept:'省司法厅',
      address:'浙江省杭州市',
      phone:'12456789038',
      count:'2',
      isConfirmed:'已确认',
      comment:'0'
    },
    {
      name:'保险1', 
      dept:'保险公司',
      address:'浙江省杭州市',
      phone:'12456789038',
      count:'2',
      isConfirmed:'未确认',
      comment:'0'
    }
  ];
  $scope.count = $scope.items.length;
}])

// 活动提醒
.controller('simplePersonWarn', ['$scope', function ($scope) {
  $scope.items = [
    {activityTitle:'关于举办“书海琴缘”省直机关单身青年钢琴训练营的报名', 
     date: '2014-8-15',
     content:'请提前报名'},
    {activityTitle:'关于开展2014年度浙江省青年岗位能手评选的通知', 
     date: '2014-8-15',
     content:'请尽快报名'}
  ];
}])

// 通讯录
.controller('simplePersonAddressBook', ['$scope', 'Data', '$stateParams', function ($scope, Data, $stateParams) {
  $scope.items = [
    {'name': '王成',
     'imageUrl': "img/defaultAvatar.png",
     'work': '省经信委',
     'address':'',
     'phone':'87052438',
     'cellphone':'15057188887'},
    {'name': '戴丽娟',
     'imageUrl': "img/defaultAvatar.png",
     'phone':'87052438',
     'cellphone':'15057188887',
     'work': '浙江长征职业技术学院',
     'address':'留和路525号'},
    {'name': '叶林伟',
     'imageUrl': "img/defaultAvatar.png",
     'work': '杭州市某自动化技术有限公司',
     'address':'杭州市拱墅区舟山东路66号',
     'phone':'',
     'cellphone':'15057188887'}
  ];

    var pageParams = 
  {
    tabCode: $stateParams.tabCode,
    loaded: 0,
    lastID: 0,
    requestNO: 20
  };
  
  // var moreData = false;

  // Data.activityList.loadlist(pageParams, function (data) {
  //   $scope.serviceLists = data.data.items;
  //   // console.log(data.data.items); //==================test
  //   pageParams.loaded = $scope.serviceLists.length;
  //   pageParams.lastID = $scope.serviceLists[$scope.serviceLists.length - 1] && $scope.serviceLists[$scope.serviceLists.length - 1][0] || 0;
  //   moreData = true;
  // });

  //下拉刷新
  $scope.doRefresh = function () {
    // pageParams.lastID = 0;
    // pageParams.requestNO = pageParams.loaded;
    // Data.activityList.loadlist(pageParams, function (data) {
    //   $scope.serviceLists = data.data.items;
    //   pageParams.loaded = $scope.serviceLists.length;
    //   pageParams.lastID = $scope.serviceLists[$scope.serviceLists.length - 1] && $scope.serviceLists[$scope.serviceLists.length - 1][0] || 0;
      $scope.$broadcast('scroll.refreshComplete');
    // });
    
  };

  //上拉加载
  $scope.loadMoreData = function () {
    // Data.activityList.loadlist(pageParams, function (data) {
    //   $scope.serviceLists = $scope.serviceLists.concat(data.data.items);
    //   // Storage.kset(pageParams[index].tabCode, $scope.articleLists[index].length);
    //   pageParams.loaded = $scope.serviceLists.length;
    //   pageParams.lastID = $scope.serviceLists[$scope.serviceLists.length - 1] && $scope.serviceLists[$scope.serviceLists.length - 1][0] || 0;
      $scope.$broadcast('scroll.infiniteScrollComplete');
    // });

    // Data.activityList.loadlist(pageParams, function (data) {
    //   if(data.data.items.length < 1) {
    //     moreData = false;
    //   }
    //   else {
    //     moreData = true;
    //   }
    //   // console.log(moreData);
    // });
    
  };
  $scope.moreDataCanBeLoaded = function () {
    return moreData;
  };

  $scope.$on('stateChangeSuccess', function () {
    $scope.loadMoreData();
  });
}])

// 聊天室
.controller('simpleChatroom', ['$scope', function ($scope) {
  var lastID = '';//声明上次取回的消息的ID
  var mGetTime;//设置setTimeout的返回值
  $scope.content = '';
  // var getMessReq = Chat.getAjax();//获取消息的XMLHTTPRequest对象
  // var sendMessReq = Chat.getAjax();//发送消息的XMLHTTPRequest对象
  // Chat.getMess(lastID, mGetTime, getMessReq);
  document.getElementById('mess').focus();//把焦点设置到消息输入框

  $scope.getMessage = function () {
    return;
  };

  $scope.sendMessage = function () {
    $scope.content = $scope.content + 'Good';
    // Chat.sendMess(lastID, mGetTime, sendMessReq);
    return;
  };
}])
TODO List
=====================
###2014.12.19-8ed026260f13a6e3dff01ba7586e85b6d14997a2

1. 出现加载过的文章列表页面再次进入时完全消失的问题

	* 去掉`$ionicView.beforeEnter`中的`init()`就好了，但是需要找到原因
	* 原因：`init()`之前没有对下面的参数进行初始化，服务器已经没数据了，返回了空数组
		```js
		pageParams.lastID = 0;
		pageParams.loaded = 0;
		pageParams.requestNO = 20;
		moreData = false;
		```

###2014.12.17-656fb014ff0d9611cf74d56c37c5c2e179f01948

升级到beta.14后需要解决的问题

1. 有一定几率ion-slide-box里面的图片会消失，代码代码里面还在，但是不显示(看最新的ion-slide-box文档)

2. 动态设置Title有问题，点击返回的时候会乱掉(非完美解决)

3. 用登录窗口登录成功，$state.go之后会出现问题，side menu打不开，按钮消失(看最新的ion-side-menus文档)？解决：`<ion-side-menus enable-menu-with-back-views="true">`，增加enable-menu-with-back-views="true"属性，原因：$state.go触发了导航事件，而enable-menu-with-back-views默认是false，导致swipe到ion-side-menu被disable，并且menu-toggle被隐藏(可以将menu-toggle改为ion-nav-back-button试验效果)

4. 不使用ion-infinite-scroll，对性能有较大的影响，直接点击按钮加载

5. 输入框要改成focus之后才会显示删除的叉叉

6. 图片懒加载(可能ionic或angular已经自带了，ngSrc？)

7. 重构程序，考虑cache的影响

8. 优化程序，减少监听事件，监听事件销毁

###2014.12.09-3fb13b4d80f895cc774a06069b35ae53f7ab7619

调优：

1. 下拉加载动画不显示（collection-repeat + ion-infinite-scroll）

2. 画面切换动画显示优化

###2014.12.07-a790c1627ef8716f05b2f51d3380e8bc8251416f

1. 真机测试，注意Connection.UNKNOWN的影响；

2. 调研$resource的timeout以及网络卡的情况(能联网但是速度慢)下的容错机制；

3. 代码调优简化

###2014.12.01-a1e6227cfdcad1c7fbed8ec69e4ed6c3434c2926

1. 离线时APP在初始页面一直加载，打不开 (要把index.html中的iframe-set-cookie注释掉，DONE!)

2. 出现不能下拉加载的情况(纯粹是网络原因)

3. 下拉一直加载，连接超时不会停止或提示

4. 图片加载一半(调用imageload插件)(DONE?)

5. 图片懒加载

思路：在$ionicPlatform.ready(function () {})中用 $state(some.page) 跳转到主页面，而不使用$urlRouterProvider.otherwise('/simple/homepage');一开始就跳转到默认主页，这么做有几个作用: 

--> 1) 可以保证deviceReady事件以及所有插件都加载完成后，再运行程序功能，保证所有功能都可以正常运行；

--> 2) 可以在程序第一次运行时，显示多个开始的滑动页面，显示完成后再点击某个链接跳转到主页，并修改ready中的$state(some.page)为$state(simple.homepage)，APP第二次运行时就直接进入主页simple.homepage；
DONE

###2014.11.29-4c8a683d1dad521f1424ba7eb02ac829d223c77e

优化imgCache与collection-repeat协同工作, buttonClearInput问题解决: TODO

1. 因为用了angular-w5c-validator (w5c-form-validate), 导致buttonClearInput指令的清除按钮在输入符合规则之前都不会出现???

###2014.11.28-524f0fadc66ec919f0fd4a2bf2747f47a9a274f3

imgCache.js done, A MileStone: TODO

1. working with collection-repeat (collection-repeat and custom);

2. 在线离线事件监听, $watch a variable?

3. 图片拍摄压缩上传;

4. 消息推送、微信转发流程设计;

5. 离线及缓存调试?

###2014.11.27-0fc6d514cc350487ef672cbc58a3b15108defc43

A MileStone: TODO

1. 在线离线事件监听，$watch a variable?

2. 图片拍摄压缩上传；

3. 消息推送、微信转发流程设计；

###2014.11.27-07ad2757629fb47abf30a47d6e46c1fcaa1ed537

TODO

1. 页面功能封装成服务；

2. 本地存储使用angular，封装成服务；

3. 页面标题动态变化；

###2014.11.23-97c34d6e302dc362fcc1813d4275e34f018dff5d

TODO List 20141123

1. 离线使用(phonegap online offline监听; 离线存储; 离线图片存储; 清除缓存...);

2. 消息推送功能完成;

3. 代码简化、压缩;

4. 图片拍照压缩上传，phonegap插件使用;

5. 从文章内容返回后定位到列表该文章标题的位置;

6. 所有页面加缓存、所有输入框加删除按钮;

7. setcookie需要设置完整的cookie形式.

文章内部排版及界面美化

搜索页面对接

内容发布页面对接，拍照上传

文章类型里面的活动评比、创业项目？

活动列表对接，活动内容对接

通讯录界面对接

聊天界面对接

功能：拍照压缩上传、消息推送、设置界面的清空缓存等功能、本地存储及定时刷新、所有的输入框都在右侧加入小叉可以点击删除框中内容

问题：
有时候登录界面出现会弹出屏幕顶部；

返回按钮还需要调整（如从/articleList/310/的一片文章返回时，会回到/articleList/而不是/articleList/310/）；

评价下拉加载更多还有问题；

从文章返回后定位到列表中该文章所在位置；

有时候会出现的乱码问题导致评论及其加载出错，编码问题；

出现一次首页不能加载的情况；

###2014.11.15-2d3806b393e6cdee5c2b06c84c5c88d67a255a31

####大量工作完成

1. 首页文章过滤（有些不属于智慧团青的文章要过滤掉）；(done)

2. 文章发布的时候图片一定要有？(done)

3. 文章内部排版问题？(done)

4. 搜索页面对接

5. 内容发布页面对接

6. 文章评论内容对接，评论发布对接(done)

7. 文章类型里面的活动评比、创业项目？

8. 活动列表对接，活动内容对接

9. 心理1解1对接

10.通讯录界面对接

11.聊天界面对接

###2014.11.11-71b0c70e3b3b875c8ded98a3aaf2a7bc42f10cc8

1. 首页图片图片加链接(done)

2. 个人定制页面不能滑动

3. 所有的main.pages都要重新加到simple.pages中（不然动画有停滞）

4. 右上角有些地方还未加搜索的放大镜，同时要去掉原本的图标

5. 文章返回不是回到栏目选择页面，而是回到文章列表页面，需要调试CustomNav（对的，不需要调整）

6. 评论和服务器对接

7. 活动列表和服务器对接

8. GPS定位功能

9. 文章转发功能加入

10.上传照片功能（压缩图像）

11.消息推送功能完成！！！

12.界面还要继续美化

13.所有的下拉框是否可以选中后下面的弹出就消失

14.文章中的内容排版问题

15.文章中图片能单独打开放大

###2014.10.26-58cd43ba911f8ca7a37e8d6d6657f9b22adddfad

文章列表服务器连接成功

1. jsonp返回数组问题，要规避半角双引号，解决方案：调研正确解析jsonp中数组的方法；

2. tab中点击进入文章，返回时回到同一tab并避免重新加载文章列表；

3. 优化上拉加载方式，避免出现多次ajax交互；

###2014.09.30-318eae4c721f4b10794f0aa09959f2353c254a0d

TODO List #3

1. $on(destroy), unbind $watch, bind, EventListener mainly in directive,
rarely in $rootScope;

2. Wechat sharing;

###2014.08.15-d498b310b2e11a49a76881caeef8a4c3abe8f1a6

TODO List #2

1. debug (setcookie...)

2. 转发到微信

3. 页面刷新

###2014.08.13-c31f62ada0cd01de3a38635e2819831d5136a952

TODO List

1. Multi-Views in .state in app.js

2. Merge templates use directive

3. Add BarScanner, GPS, Share to Weixin, Contact acquire
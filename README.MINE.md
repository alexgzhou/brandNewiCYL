﻿程序开发档案
=========================

##1======================

###Subject   : 微信分享功能开发(Android版), !!!!!!!!!直接使用nl.x-services.plugins.socialsharing插件就可以!!!!!!!!!

Date      : 20140815

Author    : Alex Zhou

Abstract  : 

Key Words : Wechat; Weixin; Phonegap; Cordova; Plugin; Share


Main Body----------------

Method & Material~~~~~~~~

Online    : http://blog.csdn.net/chen1026241686/article/details/38368713

            https://github.com/xu-li/cordova-plugin-wechat

            https://github.com/xu-li/cordova-plugin-wechat/issues/1

            https://github.com/xu-li/cordova-plugin-wechat-example

Code      : cordova plugin add https://github.com/xu-li/cordova-plugin-wechat

            https://open.weixin.qq.com/cgi-bin/frame?t=resource/res_main_tmpl&verify=1&lang=zh_CN&target=res/app_download_android   ##Android开发工具包中的libammsdk.jar, 在externalLibs/中


Procedure~~~~~~~~~~~~~~~~

1. 需要完全配置好Android-SDK开发环境(包括SDK, git, apache-ant, Java等, 见？文档);

2. 建好工程(ionic或cordova命令);

3. `$ cordova plugin add https://github.com/xu-li/cordova-plugin-wechat` (同时安装好其他插件);

4. `$ cordova platform add android` (执行本操作后, 会把所有需要的资源加入到platforms/android/目录中, 包括www\config.xml, plugins\*, res\*, 等等, 所以下面的步骤#7和#8都应该在本步骤之前完成);

5. 将libammsdk.jar放置到\platforms\android\CordovaLib\libs和\platforms\android\libs;

6. `$ cordova build` (先编译一下试试看!), 编译出错, 按照错误提示作如下修改:

7. 修改\plugins\xu.li.cordova.wechat\src\android\Wechat.java和\platforms\android\src\xu\li\cordova\wechat\Wechat.java (本文件可以不修改), 即Android插件代码;

7.1 将line:23@ `public class Weixin extends CordovaPlugin {` 中的`Weixin`改为`Wechat`, 插件作者有误;

7.2 将line:17@ `import com.tencent.mm.sdk.openapi.SendMessageToWX;` 中的`openapi`改为`modelmsg`, 微信接口更新, 查阅Android_SDK.zip中的文档发现;

7.3 将line:19, 20, 21@ 作相同修改;

7.4 将line:25@ `public static final String WXAPPID_PROPERTY_KEY = "weixinappid";` 中的`weixinappid`改为`wechatappid`, 插件作者笔误;

7.5 将line:1@ `package xu.li.cordova.Wechat;` 中的`Weixin`改为`Wechat`;

8. 在\www\config.xml中加入`<preference name="wechatappid" value="wx427f444432aef6cc" />;` 不要在\platforms\android\res\xml\config.xml中加, 每次`$ ionic build`或`$ cordova build`后, 这个config.xml都会被\www\config.xml覆盖; 这里的`wechatappid`和上面的`WXAPPID_PROPERTY_KEY`对应;

9. 再次编译(`$ cordova build`或`$ ionic build`);

10. 使用方法: 

```js
Wechat.share({
    message: {
       title: "Message Title",
       description: "Message Description(optional)",
       mediaTagName: "Media Tag Name(optional)",
       thumb: "http://YOUR_THUMBNAIL_IMAGE",
       media: {
           type: Wechat.Type.WEBPAGE,   // webpage
           webpageUrl: "https://github.com/xu-li/cordova-plugin-wechat"    // webpage
       }
   },
   scene: Wechat.Scene.TIMELINE   // share to Timeline
}, function () {
    alert("Success");
}, function (reason) {
    alert("Failed: " + reason);
});
```

11. `$ cordova emulate android`或`$ ionic emulate android`;


Result~~~~~~~~~~~~~~~~~~~

出错: `alert("Failed: " + reason) == alert('Failed: Class not found');`

解决方案: 待解决;

其他方案: !!!!!!!!!直接使用nl.x-services.plugins.socialsharing插件就可以!!!!!!!!!

##1-END==================

##2======================

###Subject   : gulp构建工具使用

Date      : 20140902

Author    : Alex Zhou

Abstract  : 034d93bb2e88b8eedfa2565f6a5ca55cdea1584f

Key Words : gulp, grunt, package.json, gulpfile.js, uglify


Main Body----------------

Method & Material~~~~~~~~

Online    : http://handyxuefeng.blog.163.com/blog/static/4545217220142264922146/

Code      : 


Procedure~~~~~~~~~~~~~~~~

注意点: 要安装好gulp任务相关的工具包, 

```bash
$ npm install gulp-util --save-dev //安装到node_modules目录下, 同时加入到package.json的devDependencies中, 如果--save-dev 改为--save, 则加入到package.json的dependencies中;
```

Result~~~~~~~~~~~~~~~~~~~

##2-END==================

##3======================

###Subject   : CSS笔记

Date      : 20140930

Author    : Alex Zhou

Abstract  : 989061b8ce8286ff6ce6a4b1e2a62b9f3019d08c

Key Words : 


Main Body----------------

1) CSS对某元素不生效的时候, 在Chrome里面找到该元素的CSS(不确定时可以直接在Chrome里面修改看效果), 然后把前面的选择器全部拷贝到自己的.css文件中(有一堆元素选择器时黑色的是生效的选择器, 灰色的都是无效的元素选择器), 肯定有效: 还是元素选择器的问题, 没有选中该元素.

2) CSS注意点: 1. CSS选择器优先级; 2. 伪元素选择器; 3. 元素选择器定位元素.

3) 一旦一个页面里面有style标签包围的css, 如`<style type="text/css"></style>`, 这个页面加载后其中的css会影响到后面所有的页面(css不会因为跳转到其他页面而失效), 和在index.html里面引用或加载效果一样.

4) 子元素会把父元素撑大, 而`div`元素本身是没有高度的, 设置成`height="100%"`, 只是占满其父元素的100%高度, 如果要设置某一个`div`占满屏幕高度, 则需要把这个`div`的所有父元素都设置成100%高度, 不然其高度只能靠其中所有的子元素撑起来, 如果子元素的高度不能占满屏幕高度100%, 则该`div`不能占满屏幕高度.

5) ionic框架的css选择器: `.platform-ios`, `.platform-android`, `.platform-cordova` 分别代表对ios平台, android平台, cordova框架生效, 可以针对平台进行css设置.

6) `:not(.someclass)`选择器表示非`.someclass`元素.

7) `white-space: normal;`表示自动换行, `white-space: nowrap;`禁止自动换行.

Method & Material~~~~~~~~

Online    : 

Code      : 

Procedure~~~~~~~~~~~~~~~~

Result~~~~~~~~~~~~~~~~~~~

##3-END==================

##4======================

###Subject   : Xcode环境下开发

Date      : 20141015

Author    : Alex Zhou

Abstract  : 7ba73bb76c3f2898ca2875e20df90981988025ee

Key Words : 


Main Body----------------

0) `$ cordova plugin add https://github.com/xu-li/cordova-plugin-wechat`

1) `$ cordove platform add ios`

2) 打开platforms/ios/iCYL beta.xcodeproj

3) `iCYL beta -> Targets -> General -> Build` 修改成100之类的数字 (optional)

4) `iCYL beta -> Targets -> Info -> URL Types` 加`weixin [weixinAPPID]` (e.g. `Identifier: weixin; URL Schemes: wx9123bbd027f0a99e`)

5) `iCYL beta -> Targets -> Build Settings -> Architectures` $(ARCHS_STANDARD)改为$(ARCHS_STANDARD_32_BIT), 或两者并存

6) iCYL beta, Plugins 右键 Add Files to "iCYL beta"...  加入微信SDK, libWeChatSDK.a, WXApi.h, WXApiObject.h (插件ajccom里有, 或者到open.weixin.qq.com下载最新, 现在有最新iOS64位版本了, 需要试验是否可以使用? 使用externalLibs文件夹里面的64位就可以, 用ajccom的SDK会在iphone 

5s以上模拟器编译中失败, 因为iphone 5s以上是纯64位处理器)

7) iCYL beta, Resources, icons & splash 右键 Add Files to "iCYL beta"... 加入icons和splash, 或者直接拷贝替换platforms/ios/iCYL beta/Resources/中的icons和splash


Method & Material~~~~~~~~

Online    : http://blog.sina.com.cn/s/blog_ad3a545d0102v0dh.html

Code      : 

Procedure~~~~~~~~~~~~~~~~

Result~~~~~~~~~~~~~~~~~~~

##4-END==================

##5======================

###Subject   : Xcode模拟器测试、真机测试、Beta测试、Archive－Organizer、常规设置

Date      : 20141015

Author    : Alex Zhou

Abstract  : 

Key Words : 

Main Body----------------


Method & Material~~~~~~~~

Online    : 

Code      : 

Procedure~~~~~~~~~~~~~~~~

Result~~~~~~~~~~~~~~~~~~~

##5-END==================

##6======================

###Subject   : WeChat插件使用

Date      : 20141015

Author    : Alex Zhou

Abstract  : af5428f57b8b34e8ed4fa02729203301f43af294

Key Words : ajccom, phonegap-weixin, xu.li, cordova-plugin-wechat


Main Body----------------

1. xu.li 见 ＃4

1) `scene: Wechat.Scene.SESSION`是分享到聊天界面; `Wechat.Scene.TIMELINE`是分享到朋友圈; `Wechat.Scene.FAVORITE`是分享到收藏

2. ajccom

1) `$ cordova plugin add https://github.com/ajccom/phonegap-weixin.git`

2) `iCYL beta -> Targets -> Info -> URL Types` 加`weixin [weixinAPPID]` DONE in #4

3) Add following code to openURL method in AppDelegate.m file:

    a) `#import "WXApi.h"`

    b) `return [WXApi handleOpenURL:url delegate:self];` in `'- (BOOL)application:(UIApplication*)application openURL:(NSURL*)url sourceApplication:'` method

    c) 注意作者github说明文档里面笔误, handleOpenURL错写成hadleOpenURL, 要改正!!!


Method & Material~~~~~~~~

Online    : 

Code      : 

Procedure~~~~~~~~~~~~~~~~

Result~~~~~~~~~~~~~~~~~~~

##6-END==================

##7======================

###Subject   : 微信分享插件(废弃)

Date      : 20141010

Author    : Alex Zhou

Abstract  : 在Mac OS X 10.9.5 + Xcode 6.0.1环境下开发微信分享功能

Key Words : 


Main Body----------------

参考问题: 今天在用一个第三方的静态库.a文件的时候, 运行于64位模拟器, 报错. 我在终端查了下那个.a文件支持哪些文件结构, 发现缺少x86_64, arm64

解决方法: 是把自己的工程只适配于32位, iPhone5s以上是64位兼容32位, 在TARGETS里面点项目名 -> Build Settings -> Architectures -> Other.. -> $(ARCHS_STANDARD_32_BIT), 这样你的App就会只支持32位的了.

我的问题: 出现WXApi.h file not found的错误, 是因为WXApi.h只支持32位, 按照上述方法修改后问题解决.


Method & Material~~~~~~~~

Online    : https://github.com/ajccom/phonegap-weixin

Code      : 


Procedure~~~~~~~~~~~~~~~~

1. 工程在Xcode中打开(参考PhoneGap文档iOS Platform Guide);

2. 在工程中设置URL Types(选中工程, Targets里面点项目名, Info);

3. 设置Build Settings里面的Architectures从`$(ARCHS_STANDARD)`改为`$(ARCHS_STANDARD_32_BIT);`

4. 在工程中导入Wechat的Lib, libWeChatSDK.a, WXApi.h, WXApiObject.h(任意文件夹上右键Add Files to "Project Name");

5. 模拟器运行, 真机运行.


Result~~~~~~~~~~~~~~~~~~~

Note~~~~~~~~~~~~~~~~~~~~~

1. Xcode工程最外面的config.xml和www文件夹是项目中的原始文件, 不是platforms目录里面的文件;

2. Staging目录里的文件是platforms目录下的文件;

3. 要修改icon和splash需要在Resources/splash和Resources/icon目录下直接修改, 修改www/res里面的文件没用;


##7-END==================

##8======================

###Subject   : iPhone连接VMWare中的Mac OS X系统

Date      : 20141010

Author    : Alex Zhou

Abstract  : VMWare暂时不识别USB3.0接口, 因此点击VMWare右下角任务栏切换设备时会出现驱动无法安装的错误, 将iPhone连接到USB2.0接口就解决了.


##8-END==================

##9======================

94e703823a1ba6aeab3909660022a374cc80cdfd

###CORS enabled:
```
1. install chrome extension: access-control-allow-origin
2. apache config Header add Access-Control-Allow-Origin * :
http://blog.csdn.net/linxiangyao/article/details/6107617
```

PhoneGap的CORS是直接enable的.

conflict in collection-repeat with directive img-cache, collection-repeat只渲染当前窗口中的item, 导致窗口以外的img-cache不起作用, 同时,
下拉增加的item中的img-cache指令(directive)也不起作用.(已解决, 采用$Observe)

https://app.yinxiang.com/shard/s9/nl/3688389/e34a5b16-fe71-4c23-bc4a-fdf380277f92/?csrfBusterToken=U%3D3847c5%3AP%3D%2F%3AE%3D149f300fcdd%3AS%3D26c5474d1a24543992d5ba2ba68420b3

http://forum.ionicframework.com/t/v1-0-0-beta-11-collection-repeat-doesnt-work-with-directives/8267/11

http://codepen.io/waynecheah/pen/wfeEo/?editors=101

##9-END==================

##10=====================

6d877424896394f78e7efaeccbbeddfb72b487a9

###iOS iframe scroll bug fix with ionic, webkit overflow

https://github.com/driftyco/ionic/issues/1151

http://davidwalsh.name/scroll-iframes-ios

```html
<ion-content scroll="true" overflow-scroll="true" class="has-header
padding">
<h1>Dashboard00</h1>
<iframe src="http://xkcd.com" style="width:100%;height:100%"></iframe>
</ion-content>
```

##10-END=================

##11=====================

26eeeef1d04252d96d965b090b9b4a89bbe84105

###setcookie done!!!

1. 在main.html中设置iframe(加载完远程资源postMessage.asp后, 由远程资源发送一个`event.data=='ready'`的postMessage给`parent.window`, 即main.html), main.html设置message监听事件, 收到ready消息后向iframe所在域发送cookie(存储在本地域localStorage中), iframe监听到message事件收到cookie后为该域设置cookie；

2. 远程域登录成功后, 设置远程域的localStorage, postMessage.asp中设置监听storage事件, 有远程域localStorage存储发生时, 将存储的值postMessage给父窗口, 父窗口的message监听到事件后判断是否为远程域localStorage且值符合要求(length>60), 如果是, 将值存入本地域localStorage, 不是, 删除本地域localStorage。


##11-END=================

##12=====================

4f0cc3b22d50314da2915d380c867038863c7af1

###Solved no statusbar in android, config.xml

Add `<preference name="fullscreen" value="false" />` in config.xml

##12-END=================

##13=====================

a57c6fa8ad72e4c2bc3926185cdb7cf148159339

###use ng-include directive to seperate footer.html

Note to add has-footer in the class of ion-content, or the iframe will
be covered by the ion-footer-bar. If we will use the iframe's fixed
footer, then no has-footer and no ng-include footer.html, if we would
like to have both fixed footer, then has-footer and ng-include
footer.html.

##13-END=================

##14=====================

88d026ac108dcaa51a4e1d47961b330aa3270475

###bugfix#tiny

从大众点评页跳转到测试页, 出现tabs回到顶部的问题, 是由于都有`.tabs-top`造成的, 给测试页的`<ion-tabs>`标签自定义了`.custom-top`到class中, bingo！！！

##14-END=================

##15=====================

0283bb0582684ad5ebb108f3e3ea96fe49546b56

###CustomNav to Tab in Tabs

```
1. $ionicTabsDelegate; $timeout;
2. $ionicTabsDelegate.select(CustomNav.fromTab); $scope.tabNav;
3. this.fromTab = 0;
4. <ion-tab on-select='tabNav()'>
```

##15-END=================

##16=====================

0d908af2a4754bf28ee51fe0d5ffd1ea3207ce6e

###bug fixed

`Session.token`不设置, 导致`$stateChangeStart`的条件判断为真, 执行`$state.go(toState);`语句, 反过来又触发`$stateChangeStart`事件, 形成死循环。

##16-END=================

##17=====================

bc69422b6f8f1cc6343fb4e5ece6d43eb5f1d763

###解决asp传json数组的问题

1. asp中用\"替换"

```asp
replace(myarry(col,row),"""","\""")
```

2. asp中再替换掉换行符, vbcrlf常量代表换行符

```asp
replace(replace(myarry(col,row),"""","\"""),vbcrlf,"")
```

3.AngularJS中用`ng-bind-html`指令加载html, newsContent存放html代码, 直接`<div>{{newsContent}}</div>`会显示源码, 正确用法如下：

```html
<div ng-bind-html="newsContent"></div>
```

问题：

asp中需要对所有的col都进行1、2的替换处理, 但是报错？

##17-END=================

##18=====================

baf7fb1a187c07ca866b0b761663b4a038192243

###一个奇怪的问题解决

问题描述：

chrome（手机、电脑）都是好的, 但是在safari下面登录后页面不会跳转, safari调试工具的console显示一直在循环校验用户名密码, 即Session.token一直是空的, 直接访问`http://17f.go5le.net/mall/index/chklogin_app.asp?a=get_token&c=user&callback=angular.callbacks._1&password=123&username=91`显示`angular.callbacks._1({err_code:0,
err_msg:"success", data:{username:"91",password:"123",
token:""}})`, 即token值为空, 因此, 虽然用户名密码校验成功了, 但是Session.token是空的, 陷入了死循环。

问题分析：

应该是浏览器缓存的问题, 一开始某一次由于网络原因token返回值为空, 后来一直返回的是浏览器缓存的结果, 导致出现此问题。

问题解决：

将

```js
Session.create(data.data.token)
```

改成

```js
Session.create(data.data.token ? data.data.token : "whatever");
```

###更好的解决方法：

服务器端只要对token进行校验, 就不会发生上述问题

##18-END=================

##19=====================

c6e02d7dfc085674c2fea232338aa5b778b712ff

###增加directive, 用于快速clear输入框中的内容

```js
.directive( "buttonClearInput", function () {
  return {
    restrict: "AE",
    scope: {
    input: "="  //这里可以直接用input获取父scope(包括兄弟元素)中ng-model的值,
    传递给本directive创建的isolate scope使用, template也属于当前isolate scope
    },
    // replace: true,   //使用replace之后, 本元素的click不能删除输入框中的内容, 原因大致可以理解为:
    父元素被替换后, scope.$apply没有执行对象
    template:"<button ng-if='input' class='button button-icon
    ion-android-close input-button padding-right'></button>",
    link: function (scope, element, attrs) {
      // console.log(scope.$id);
      // console.log(attrs.class);
      element.bind( "click", function () {
        scope.$apply(function () {
          // scope.item.searchContent = '';
          scope.input = "";
        });
      });
    }
  };
})
```

```html
<button-clear-input input="item.searchContent"></button-clear-input>
```

##19-END=================

##20=====================

6b4bc08b831a76013d7b86bb5c9bff080a2fef88

###cookie done

chklogin_app.asp里面已经设置cookie了, 不需要再设置了, 可以完全抛弃postMessage.asp的方法以及directive中的iframeSetCookie.

所有的活动页面都需要用iframe封装！！！

备注： 要把index.html中的`iframe-set-cookie`注释掉, 不然飞行模式(完全离线)的时候会出现iOS APP在splashscreen界面一直加载而不能进入APP的问题！！！但是, 只要连了WIFI, 即使WIFI不能连上`http://17f.go5le.net/postMessage.asp`, 也能正常进入APP主页, 很奇怪？？？

##20-END=================

##21=====================

84cbe8b894954a585e06c41b9b8c86b1040f3d2d

###登录框login.html弹出顶部解决

```html
<input type="text" placeholder="请输入用户名" ng-model="loginData.username" required="" name="username" ng-pattern="/^[0-9A-Za-z_]{1,19}$/">
<!-- 在input里面加 autofocus="" 属性有可能会导致页面向上弹出顶部 -->
```

##21-END=================

##22=====================

776752b80c2fcacf706ac2538991742cd2ffb814

###DONE $observe working with collection-repeat

最简单的方法：

1. $observe可以直接检测directive所在元素的属性, 可以包含"{{}}", $watch对象不可以监控包含"{{}}"的对象;

2. 不需要在app.js的`angular.module.run()`中广播imgCache的ImgCacheReady事件;

3. 在directive的imgCache中直接调用attrs.$observe('ngSrc', ngSrcObservCallback);不用加任何判断.

修改：

问题描述: 由于ImgCache.init没有完成, 导致刷新或第一次打开页面时没有加载cached的图像(ngSrcObservCallback运行失败);

解决方法: 

1. 需要在app.js的`angular.module.run()`中广播imgCache的ImgCacheReady事件;

2. 除了在directive的imgCache中直接调用`attrs.$observe('ngSrc', ngSrcObservCallback);`之外, 同时设置:

```js
scope.$on('ImgCacheReady', function () {
  attrs.$observe('src', ngSrcObservCallback);
}, false);
```

保证不遗漏ImgCacheReady之前加载的图像.


##22-END=================

##23=====================

4c8a683d1dad521f1424ba7eb02ac829d223c77e

###表单按钮默认类型`type="submit"`

表单里面的按钮必须指定`type="button"`, 不指定就是默认的`type="submit"`

button element in form without a type attribute act as `type="submit"` by default, need to specify the `type="button"` to avoid submit when click the button

##23-END=================

##24=====================

4c8a683d1dad521f1424ba7eb02ac829d223c77e

###优化[imgCache](https://github.com/chrisben/imgcache.js)与collection-repeat协同工作； clearInput解决

`.directive("buttonClearInput", function(){})`在label元素中点击无效

原因: 

1. 和其他都无关, 而是在ionicframework框架里面`<label>`标签中的button按钮点击无效(不论是onclick还是ng-clikc还是link中element.bind click事件), 要改成`<div>`标签;

2. 不用任何框架的时候, `<label>`标签中的button按钮点击有效;

还存在一个问题: 

    因为用了[angular-w5c-validator](https://github.com/why520crazy/angular-w5c-validator), 导致buttonClearInput指令的清除按钮在输入符合规则之前都不会出现???

原因: 

    input中的输入值在符合规则之前都不会被赋值给ng-model中的变量, 而指令buttonClearInput是通过`input="ng-model变量"`来判断是否显示按钮, 因此在输入符合规则之前都不会出现.

解决方法:
    

##24-END=================

##25=====================

57799c0869bfbe12016330c05be53f7b851b1da4

###Some meaningful thought! and an example of plist

思路：

在`$ionicPlatform.ready(function () {})`中用`$state(some.page)`跳转到主页面, 而不使用`$urlRouterProvider.otherwise('/simple/homepage');`一开始就跳转到默认主页, 这么做有几个作用: 

--> 1) 可以保证deviceReady事件以及所有插件都加载完成后, 再运行程序功能, 有效控制APP的时序流程, 保证所有功能都可以正常运行；

--> 2) 可以在程序第一次运行时, 显示多个开始的滑动页面, 显示完成后再点击某个链接跳转到主页, 并修改ready中的`$state(some.page)`为`$state(simple.homepage)`, APP第二次运行时就直接进入主页simple.homepage；

--> 注意：要确保`$ionicPlatform.ready(function () {})`中执行的功能尽可能少, 并且没有阻塞进程的语句。

--> 已实现


##25-END=================

##26=====================

ce47e1783ed4ddda6afb7418b82cb9455bc12d69

###initSplashes页面跳转后出现显示问题——homepage页面collection-repeat加载不全, 下拉不刷新

原因：

  是initSplashes.html里面的style的div高度设置成100%造成的；

分析：

  一旦一个页面里面有style标签包围的css, 如`<style type="text/css"></style>`, 这个页面加载后其中的css会影响到后面所有的页面(css不会因为跳转到其他页面而失效), 和在index.html里面引用或加载效果一样；

css工作：

  /*要把所有的div都设置成100%高度, 本质是要把所有的父元素都设置成100%高度, 不然光设置某一个div, 会受父元素的影响, 不能占满屏幕高度*/

  /*这里不能对所有的div都设置高度, 会影响到后面所有的页面, 需单独设置一个元素fullheight*/

  /*ion-slide-box和ion-slide必须设置高度, 否则会影响到其内部的元素, 造成ion-header-bar和ion-content都不显示*/


##26-END=================

##27=====================

351887cd475b4906398331bba2eb969f5ed2c3d6

###用`$scope.func = function(){};`定义的函数必须在.html页面中被调用, 不能直接在.js中调用！！！

问题：

  在.js中定义了函数`$scope.init = function(){};`, 然后在.js中直接调用`$scope.init();`, 出现下拉无法加载以及`$scope.items`数据都有了但是列表不显示等问题；

解决：

  这里不能定义为`$scope.init = function(){};`, 必须定义为`var init = function(){};`, 然后直接`init();`调用函数；line 500

原因：

  `$scope.init()`必须在html中被调用, 不能直接在js程序里面被调用, 不然会出现上述奇怪的问题, 可能因为带$scope的函数作用域是.html中的DOM；line 546
  不能直接在`$scope.$on('onOnline', function () {});`中调用`$scope.doRefresh()`, `$scope.items`数据都有了, 但是会出现列表不显示的问题, 原因同上；line 673

文件：

```file
  www/js/services.js的line 500, line 546, line 673
```

##27-END=================

##28=====================

351887cd475b4906398331bba2eb969f5ed2c3d6

###chrome console在线离线调试方法

1. chrome自带网络环境模拟器(`F12 -> Toggle device mode`)；

2. chrome Console命令行模拟(在chrome F12的命令行输入以下指令)：

```bash
> $root = angular.element($0).injector().get('$rootScope'); //获取$rootScope
> $root.myOnline = false; //不用解释
> $root.$broadcast('onOffline');  //广播onOffline事件(AngularJS的功能：自定义事件)

> $root.myOnline = true;
> $root.$broadcast('onOnline');
```

##28-END=================

##29=====================

b488947335ef46e3b2ed953f305a41357eea3c54

###[.gitignore实例](http://git-scm.com/docs/gitignore)

Example to exclude everything except a specific directory foo/bar (note the `/*` - without the slash, the wildcard would also exclude everything within `foo/bar`):

```bash
$ cat .gitignore

# exclude everything except directory foo/bar
/*
!/foo
/foo/*
!/foo/bar
```

说明: `/images` 和 `images/` 是不同的, 前者代表 `.gitignore` 所在目录的 `images` 文件夹(不包括任何子目录下的 `images` 文件夹), 后者表示 `.gitignore` 所在目录以及所有子目录中的 `images` 文件夹.

##29-END=================

##30=====================

7049066d5bbec29380677c3faddcbf5dda177f5a

###Ionicons贴士

1. 在一些动词后面加ing可以使图标动起来, 如`ion-load-a`变成`ion-loading-a`, 会使图标转起来;

2. 待续...

##30-END=================

##31=====================

ad30f0ee28779e8a953f56a79ab227b2226518e9

###如果Chrome的console中出现如下错误, 可以直接打开该链接, 会跳转到angular的文档页面, 里面会描述错误的位置(注意下面链接中的"app.js:628:24", 表示了错误位置), 以及可能的解决方法！！！

[Example: https://docs.angularjs.org/error/$injector/modulerr?p0=icyl&p1=TypeError:%20undefined%20is%20not%20a%20function%0A%20%20%20%20at%20http:%2F%2Flocalhost%2Fnewicyl%2Fjs%2Fapp.js:628:24%0A%20%20%20%20at%20Object.e%20%5Bas%20invoke%5D%20(http:%2F%2Flocalhost%2Fnewicyl%2Flib%2Fionic%2Fjs%2Fionic.bundle.min.js:68:19)%0A%20%20%20%20at%20d%20(http:%2F%2Flocalhost%2Fnewicyl%2Flib%2Fionic%2Fjs%2Fionic.bundle.min.js:66:222)%0A%20%20%20%20at%20http:%2F%2Flocalhost%2Fnewicyl%2Flib%2Fionic%2Fjs%2Fionic.bundle.min.js:66:346%0A%20%20%20%20at%20r%20(http:%2F%2Flocalhost%2Fnewicyl%2Flib%2Fionic%2Fjs%2Fionic.bundle.min.js:38:302)%0A%20%20%20%20at%20g%20(http:%2F%2Flocalhost%2Fnewicyl%2Flib%2Fionic%2Fjs%2Fionic.bundle.min.js:66:123)%0A%20%20%20%20at%20Ob%20(http:%2F%2Flocalhost%2Fnewicyl%2Flib%2Fionic%2Fjs%2Fionic.bundle.min.js:69:360)%0A%20%20%20%20at%20d%20(http:%2F%2Flocalhost%2Fnewicyl%2Flib%2Fionic%2Fjs%2Fionic.bundle.min.js:48:350)%0A%20%20%20%20at%20sc%20(http:%2F%2Flocalhost%2Fnewicyl%2Flib%2Fionic%2Fjs%2Fionic.bundle.min.js:49:153)%0A%20%20%20%20at%20Hd%20(http:%2F%2Flocalhost%2Fnewicyl%2Flib%2Fionic%2Fjs%2Fionic.bundle.min.js:47:483](https://docs.angularjs.org/error/$injector/modulerr?p0=icyl&p1=TypeError:%20undefined%20is%20not%20a%20function%0A%20%20%20%20at%20http:%2F%2Flocalhost%2Fnewicyl%2Fjs%2Fapp.js:628:24%0A%20%20%20%20at%20Object.e%20%5Bas%20invoke%5D%20(http:%2F%2Flocalhost%2Fnewicyl%2Flib%2Fionic%2Fjs%2Fionic.bundle.min.js:68:19)%0A%20%20%20%20at%20d%20(http:%2F%2Flocalhost%2Fnewicyl%2Flib%2Fionic%2Fjs%2Fionic.bundle.min.js:66:222)%0A%20%20%20%20at%20http:%2F%2Flocalhost%2Fnewicyl%2Flib%2Fionic%2Fjs%2Fionic.bundle.min.js:66:346%0A%20%20%20%20at%20r%20(http:%2F%2Flocalhost%2Fnewicyl%2Flib%2Fionic%2Fjs%2Fionic.bundle.min.js:38:302)%0A%20%20%20%20at%20g%20(http:%2F%2Flocalhost%2Fnewicyl%2Flib%2Fionic%2Fjs%2Fionic.bundle.min.js:66:123)%0A%20%20%20%20at%20Ob%20(http:%2F%2Flocalhost%2Fnewicyl%2Flib%2Fionic%2Fjs%2Fionic.bundle.min.js:69:360)%0A%20%20%20%20at%20d%20(http:%2F%2Flocalhost%2Fnewicyl%2Flib%2Fionic%2Fjs%2Fionic.bundle.min.js:48:350)%0A%20%20%20%20at%20sc%20(http:%2F%2Flocalhost%2Fnewicyl%2Flib%2Fionic%2Fjs%2Fionic.bundle.min.js:49:153)%0A%20%20%20%20at%20Hd%20(http:%2F%2Flocalhost%2Fnewicyl%2Flib%2Fionic%2Fjs%2Fionic.bundle.min.js:47:483)

##31-END=================

##32=====================

cd5cd89101abfd4b6b605dca04789e6c84331627

###[Bower依赖管理工具](http://bower.io/)

[install-bower](http://bower.io/#install-bower)

[bower.json](http://bower.io/docs/creating-packages/#bowerjson)

[config](http://bower.io/docs/config/)

```bash
$ bower init
$ bower install <packageName#version> --save
$ bower install
```


##32-END=================

##33=====================

cd5cd89101abfd4b6b605dca04789e6c84331627

###ionic.io项目管理, 可以使用ionic命令来管理、编译、更新APP

[ionic.io主页](http://ionic.io/)


##33-END=================

##34=====================

219b2f89956fb60e89eaa7ed0ce50cb62e125dba

###ion-tabs、ion-tab、ion-header-bar、ion-content的正确嵌套方式(版本beta.14开始?)

```js
<ion-tabs class="tabs-striped tabs-top tabs-icon-left tabs-positive">
  <ion-tab title="我的留言" icon-on="ion-ios7-chatbubble" icon-off="ion-ios7-chatbubble-outline">
    <ion-header-bar><!-- 必需在这里加上header标签, 否则下面的content会从顶部开始, 内容会被盖住 -->
    </ion-header-bar>
    <ion-content class="has-header has-tabs">
      <ion-list>
        <ion-item ng-repeat="item in items">
          <h2>To: {{item.name}}<span class="item-note">{{item.date}}</span></h2>
          <p>内容：{{item.message}}</p>
        </ion-item>
      </ion-list>
    </ion-content>
  </ion-tab>

  <ion-tab title="我的回复" icon-on="ion-ios7-chatboxes" icon-off="ion-ios7-chatboxes-outline">
    <ion-header-bar>
    </ion-header-bar>
    <ion-content class="has-header has-tabs">
    </ion-content>
  </ion-tab>
</ion-tabs>
```

###beta.14问题汇总

####1. [enable-menu-with-back-views](http://ionicframework.com/docs/api/directive/ionSideMenus/)属性导致ion-side-menu打不开, menu-toggle按钮消失

#####1）问题：

用登录窗口登录成功并`$state.go`跳转之后会出现问题(任何用`$state.go`跳转到没有ion-nav-back-button的页面都会出现相同的问题), ion-side-menu打不开, menu-toggle按钮消失(看最新的[ion-side-menus](http://ionicframework.com/docs/api/directive/ionSideMenus/)文档)？

#####2）解决：

```html
<ion-side-menus enable-menu-with-back-views="true">
```

增加`enable-menu-with-back-views="true"`属性；

#####3）原因：

`$state.go`触发了导航事件, 而enable-menu-with-back-views默认是false, 导致swipe到ion-side-menu被disable, 并且menu-toggle被隐藏(可以将menu-toggle改为ion-nav-back-button试验效果)；

#####4）改进：

使用强大的[$ionicHistory](http://ionicframework.com/docs/api/service/$ionicHistory/)服务, 在`$state.go`之前加上如下代码-->

```js
$ionicHistory.nextViewOptions({
  disableBack: true,  //下一个页面没有back view
  historyRoot: true //下一个页面是root view in its history stack
});
```

作了上面的操作后(需要对一些页面做一下判断, 是否需要执行上述操作, 如.state里面对需要登录的页面中的access对象增加menuToggle: true属性, 有这个属性的就执行上述操作), 就可以设置`enable-menu-with-back-views="false"`, 这样可以确保有back view的页面不能打开ion-side-menu, 同时, 用`$state.go`跳转的页面保持swipe到ion-side-menu是enabled, 并且menu-toggle是显示的。

####2. `$ionicHistory.currentTitle()`和`$ionicNavBarDelegate.title()`

#####1）`$ionicHistory.currentTitle()`获取和设置的是`<ion-view>`标签中的`view-title`属性值, `$ionicNavBarDelegate.title()`获取和设置的是`<ion-nav-bar>`中的title(如`<ion-nav-title></ion-nav-title>`中的内容)；

#####2）`$ionicHistory.currentTitle('Title')`设置的`view-title`在网页上不会显示Title值, 只能用`console.log($ionicHistory.currentTitle())`查看到, 而直接在.html中写死的`view-title="Title"`会显示；

#####3）由于在beta.14中的`<ion-nav-bar>`标签下会有两个`<div class="nav-bar-block" nav-bar="cached">`, 也就是有两个`<ion-header-bar>`, 其中有一个会被`nav-bar="cached"`缓存, 如前所示, 导致第二次进入某一个页面的时候, 在.html里面写死的`view-title="Title"`也不会显示, 而第一次以及第三次点击进去都会正常显示, 唯独第二次不显示；

  * **解释：上述问题`3）`是由于开启了`$ionicConfigProvider.views.forwardCache(true);`, 缓存了前向网页的原因, 关闭就可以**

- **完整问题及解决方案**

  * (1) 问题：动态设置`$ionicNavBarDelegate.title('Title')`出现返回再进入后标题错乱的问题, 见[commit: 05d8be10cb078105e63bd10a7d89dfc6209c8e01](https://github.com/alexgzhou/brandNewiCYL/commit/05d8be10cb078105e63bd10a7d89dfc6209c8e01)中的`#/simple/navArticle`页面；

  * (2) 分析：beta.14缓存了`nav-bar="cached"`, 导致前一个返回页面的view-title被缓存, 影响下一个页面的view-title

  * (3) 解决：见[commit: 18301e7b5a10e9e79c2a7f22e7c0c315a2c4633a](https://github.com/alexgzhou/brandNewiCYL/commit/18301e7b5a10e9e79c2a7f22e7c0c315a2c4633a)中article.html、articlelist.html、articleNav.html以及homepage.html的`view-title`

  * (4) 还存在问题：从文章页返回后标题变成写死在`<ion-nav-title>Title</ion-nav-title>`或者`view-title="Title"`里面的Title, [commit: 18301e7b5a10e9e79c2a7f22e7c0c315a2c4633a](https://github.com/alexgzhou/brandNewiCYL/commit/18301e7b5a10e9e79c2a7f22e7c0c315a2c4633a), 还是重新打开`$ionicConfigProvider.views.forwardCache(true);`, 至少从文章返回后, Title是对的, 注意articlelist.html等list文件中不要设置任何写死的标题, 包括`<ion-nav-title>Title</ion-nav-title>`或者`view-title="Title"`



##34-END=================

##35=====================



###事件监听/解除监听(绑定/解除绑定)

- **Angular:**
  * $scope.$on

    ```js
    var release = $scope.$on('eventHandler', callback); //监听
    release();  //解除

    //或者由$destory事件触发解除
    $scope.$on("$destroy", function (e) {
      release();
    });
    ```

  * element.bind in directives' link -- 和jQuery的方式一样, 因为Angular用了jQuery Lite
    
    ```js
    element.bind('eventHandler', callback); //监听
    element.unbind("eventHandler", callback);  //解除：callback不能用匿名函数代替
    
    //或者由$destory事件触发解除
    scope.$on("$destroy", function (e) {
        element.unbind("eventHandler", callback);
    });
    ```

  * unbind $scope.$watch

    ```js
    var unbindWatcher = $scope.$watch('somethingWatched', callback); //监听
    unbindWatcher();  //解除
    ```

  * unbind $scope.$observe -- Angular 1.3.x 才支持
  
    ```js
    var unbindObserve = attrs.$observe('somethingObserved', callback); //监听
    unbindObserve();  //解除
    ```

##35-END=================

##36=====================

0415ec89da152c5ee94fab1cb3d14f691f43609d

###针对android调优, css笔记, android平台chrome自带navigator.connection对象

1. 针对android平台(.platform-android), 将右边按钮组的最小宽度设置为100px, 否则在android 4.4.x原生浏览器上, header右边按钮组中放两个按钮就会换行显示: 
  ```css
  .platform-android .buttons-right .right-buttons { 
      min-width: 100px;
  }
  ```

2. android 4.4.2版本上安装的chrome浏览器自带`navigator.connection`对象, 不需要phonegap/cordova环境就有`navigator.connection`, 导致Connection.NONE未定义: `app.js line 21`.

##36-END=================

##37=====================

b62c47652c06a4e65c6a561a9289960124361386

###PhoneGap & Cordova 新进展

1. phonegap和cordova都是CLI工具, 前提是安装相应的npm包：
  ```bash
  $ npm install -g cordova phonegap

  $ cordova create myApp
  # or
  $ phonegap create myApp
  ```

2. [PhoneGap Developer App 调试工具](http://app.phonegap.com/), 以及图形化工具[phonegap-desktop-app-beta](http://phonegap.com/blog/2014/12/11/phonegap-desktop-app-beta/)

  - 1) 安装phonegap
    ```bash
    $ npm install -g phonegap
    ```

  - 2) 下载安装PhoneGap Developer App
    * [苹果商店](https://itunes.apple.com/app/id843536693)
    * 安卓APP在[这里](https://github.com/alexgzhou/brandNewiCYL/tree/NeatAndTidy/externalLibs_Bin/phonegap_developer/com.adobe.phonegap.app.apk)

  - 3) 创建App并启动服务器, 注意：在这一步骤中, 由于GFW的存在, 导致myApp文件夹都产生了, 但是里面没文件(git上的模板没成功下载), 需要将这个[test_phonegap.zip](https://github.com/alexgzhou/brandNewiCYL/tree/NeatAndTidy/externalLibs_Bin/phonegap_developer/test_phonegap.zip)文件解压, 拷贝里面所有的文件到myApp目录中, 下面的步骤可以成功进行; 下面的命令也可以使用图形化工具[phonegap-desktop-app-beta](http://phonegap.com/blog/2014/12/11/phonegap-desktop-app-beta/)完成, 官网下载困难, [这里有win版](https://github.com/alexgzhou/brandNewiCYL/tree/NeatAndTidy/externalLibs_Bin/phonegap_developer/PhoneGap-Desktop-Beta-0.1.1-win.zip)
    ```bash
    $ phonegap create myApp
    $ cd myApp/
    $ phonegap serve
    listening on ip_address:3000/unknown:3000
    ```

3. cordova version 4.x 版本新增了browser平台, [介绍](http://www.raymondcamden.com/2014/9/24/Browser-as-a-platform-for-your-PhoneGapCordova-apps), [on GitHub](https://github.com/apache/cordova-browser), 目前还不成熟(2014.12.22)：

  - 1) 增加平台
    ```bash
    $ cordova platform add browser --usegit
    ```

  - 2) 关闭Chrome浏览器
    close Chrome completely, 要用命令行带参数重新打开Chrome

  - 3) 运行平台
    ```bash
    $ cordova run browser
    ```


##37-END=================

##31=====================

###Android程序签名

##31-END=================

##31=====================

###Android程序签名

##31-END=================

##31=====================

###Android程序签名

##31-END=================

##31=====================

###Android程序签名

##31-END=================

##31=====================

###Android程序签名

##31-END=================
// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js

var dependencies = ['ionic',
                    'icyl.services',
                    'icyl.directives',
                    //'icyl.filters',
                    'icyl.controllers',
                    'w5c.validator'];

var myAppVersion = '0.3.0';

// 测试专用
if (!navigator.connection) {
  var Connection = {
    NONE: false
  };
}

angular.module('icyl', dependencies)

.run(['$ionicPlatform', '$rootScope', '$state', 'Storage', '$ionicPopup', function ($ionicPlatform, $rootScope, $state, Storage, $ionicPopup) {

  var pushNotification;

  // handle APNS notifications for iOS
  function onNotificationAPN(e) {
    if (e.alert) {
      // showing an alert also requires the org.apache.cordova.dialogs plugin
      $ionicPopup.alert({title: '收到消息APN', template: e.alert, okText: '确认'});
    }
        
    if (e.sound) {
      // playing a sound also requires the org.apache.cordova.media plugin
      var snd = new Media(e.sound);
      snd.play();
    }
    
    if (e.badge) {
      pushNotification.setApplicationIconBadgeNumber(successHandler, e.badge);
    }
  }
  
  // handle GCM notifications for Android
  function onNotification(e) {
    switch( e.event ) {
      case 'registered':
        if ( e.regid.length > 0 )
        {
          // Your GCM push server needs to know the regID before it can push to this device
          // here is where you might want to send it the regID for later use.
          console.log("regID = " + e.regid);
        }
      break;
          
      case 'message':
        // if this flag is set, this notification happened while we were in the foreground.
        // you might want to play a sound to get the user's attention, throw up a dialog, etc.
        if (e.foreground)
        {
          // on Android soundname is outside the payload. 
          // On Amazon FireOS all custom attributes are contained within payload
          var soundfile = e.soundname || e.payload.sound;
          // if the notification contains a soundname, play it.
          // playing a sound also requires the org.apache.cordova.media plugin
          var my_media = new Media("/android_asset/www/"+ soundfile);
          my_media.play();
        }
        else
        { // otherwise we were launched because the user touched a notification in the notification tray.
          if (e.coldstart)
            $ionicPopup.alert({title: '收到消息GCM-0', template: e.payload.message, okText: '确认'});
          else
            $ionicPopup.alert({title: '收到消息GCM-1', template: e.payload.message, okText: '确认'});
        }
    
        $ionicPopup.alert({title: '收到消息GCM', template: e.payload.message, okText: '确认'});
        //android only
        $ionicPopup.alert({title: '收到消息GCM-2', template: e.payload.msgcnt, okText: '确认'});
        //amazon-fireos only
        $ionicPopup.alert({title: '收到消息GCM-3', template: e.payload.timeStamp, okText: '确认'});
      break;
          
      case 'error':
        $ionicPopup.alert({title: '收到消息GCM-err', template: e.msg, okText: '确认'});
      break;
          
      default:
        $ionicPopup.alert({title: '收到消息GCM-default', template: 'EVENT -> Unknown', okText: '确认'});
      break;
    }
  }
  
  function tokenHandler (result) {
    // $("#app-status-ul").append('<li>token: '+ result +'</li>');
    // navigator.notification.alert(result, undefined, 'PushToken', '确认');
    $ionicPopup.alert({title: 'PushToken', template: result, okText: '确认'});
    // Your iOS push server needs to know the token before it can push to this device
    // here is where you might want to send it the token for later use.
  }

  function successHandler (result) {
    // $("#app-status-ul").append('<li>success:'+ result +'</li>');
    $ionicPopup.alert({title: 'successHandler', template: result, okText: '确认'});
  }
  
  function errorHandler (error) {
    // $("#app-status-ul").append('<li>error:'+ error +'</li>');
    $ionicPopup.alert({title: 'errorHandler', template: error, okText: '确认'});
  }

  //deviceReady事件
  $ionicPlatform.ready( function () {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard for form inputs)
    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if(window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
      // StatusBar.overlaysWebView(false); //iOS6 style
      // StatusBar.styleLightContent();
    }
    //window.ionic.Platform.showStatusBar(false)
    //window.ionic.Platform.fullScreen(true,false);
    
    // console.log(window.plugins);
    if (window.plugins && window.plugins.pushNotification) {
      try 
      { 
        pushNotification = window.plugins.pushNotification;
        if (device.platform == 'android' || device.platform == 'Android' || device.platform == 'amazon-fireos' ) {
          pushNotification.register(successHandler, errorHandler, {"senderID":"642769024933","ecb":"onNotification"});    // required!
        } 
        else {
          pushNotification.register(tokenHandler, errorHandler, {"badge":"true","sound":"true","alert":"true","ecb":"onNotificationAPN"});  // required!
        }
      }
      catch(err) 
      { 
        txt="\nThere was an error on this page.\n"; 
        txt+="Error description: " + err.message + "\n"; 
        console.log(txt); 
        $ionicPopup.alert({title: 'PushInit', template: txt, okText: '确认'});
      } 
    }

    // set up and init image caching
    // write log to console
    ImgCache.options.debug = false;
    // increase allocated space on Chrome to 50MB, default was 10MB
    ImgCache.options.chromeQuota = 50*1024*1024;
    // size in MB that triggers cache clear on init, 0 to disable
    ImgCache.options.cacheClearSize = 100;
    ImgCache.init(function(){
      //small hack to dispatch an event when imgCache is full initialized.
      $rootScope.$broadcast('ImgCacheReady');
      // $ionicPopup.alert({title: 'ImgCacheInit', template: 'ImgCache init: Success', okText: '确认'});
    }, function(){
      $ionicPopup.alert({title: 'ImgCacheInit-Err', template: 'ImgCache init: error!', okText: '确认'});
    });

    // $ionicPopup.alert({title: '网络测试', template: navigator.connection.type, okText: '确认'});
    if (navigator.connection) {
      $rootScope.myOnline = navigator.connection.type;
    }
    else {
      $rootScope.myOnline = window.navigator.onLine;
    }

    $ionicPlatform.on('online', function () {
      // $ionicPopup.alert({template: 'UNKNOWN: ' + Connection.UNKNOWN + "\n\r" +
      //                   'ETHERNET: ' + Connection.ETHERNET + "\n\r" +
      //                   'WIFI: ' + Connection.WIFI + "\n\r" +
      //                   'CELL_2G: ' + Connection.CELL_2G + "\n\r" +
      //                   'CELL_3G: ' + Connection.CELL_3G + "\n\r" +
      //                   'CELL_4G: ' + Connection.CELL_4G + "\n\r" +
      //                   'CELL: ' + Connection.CELL + "\n\r" +
      //                   'NONE: ' + Connection.NONE + "\n\r", 
      //                   undefined, '在线事件', '确认'}); //================for Test
      if (navigator.connection) {
        $rootScope.myOnline = navigator.connection.type;
      }
      else {
        $rootScope.myOnline = window.navigator.onLine;
      }
      $rootScope.$broadcast('onOnline');
    }, false);
    $ionicPlatform.on('offline', function () {
      // $ionicPopup.alert({title: '离线事件', template: navigator.connection.type, okText: '确认'}); //================for Test
      if (navigator.connection) {
        $rootScope.myOnline = navigator.connection.type;
      }
      else {
        $rootScope.myOnline = window.navigator.onLine;
      }
      $rootScope.$broadcast('onOffline');
    }, false);
    
    // 首次进入程序进入initSplashes页面
    if (myAppVersion !== Storage.kget('myAppVersion')) {
      $state.go('initSplashes');
    }
    else {
      $state.go('simple.homepage');
    }
    // var initState = Storage.kget('initState') ? Storage.kget('initState') : 'initSplashes';
    // $state.go(initState);
  });

}])

.run(['$rootScope', '$state', 'Identification', 'User', 'Session', '$ionicPopup', function ($rootScope, $state, Identification, User, Session, $ionicPopup) {
  $rootScope.$on("$stateChangeStart", function (event, toState, toParams, fromState, fromParams){
    // console.log("stateChangeStart入口: '" + fromState.name + "' -> '" + toState.name + "'; +'" + Session.token + "'");
    // console.log(fromState);
    if (toState.access.authenticate && !Identification.isAuthenticated()) {
      // User isn’t authenticated
      //event.preventDefault();
      //console.log("#1----------"+toState.access.offline+"==="+fromState.name+"=--="+(toState.access.offline != fromState.name));  //=====================test
      // console.log("OK");
      if (!!toState.access.offline && toState.access.offline != fromState.name) {
        $state.go(toState.access.offline);
      }
      else {
        // console.log("checkToken入口: '" + fromState.name + "' -> '" + toState.name + "'; + '" + !Session.token + "'");
        Identification.checkToken().then( function (data) {
          if (data.err_code !== 0) {
            $rootScope.actions = {
              toState: toState
            };
            //$rootScope.actions.toState = toState;
            User.userLogin($rootScope);
            User.userRegister($rootScope);
            //console.log("#1----------"+$rootScope.$id+"=="+data.err_code);  //=====================test
            //因为在User.userLogin里面$ionicModal.fromTemplateUrl是异步加载模板的，所以在这里如果直接调用的话会出错(还没加载完成)；
            //解决方法一：改造成promise形式；
            //解决方法二：直接在$ionicModal.fromTemplateUrl(url).then($scope.loginmodal = modal; $scope.loginmodal.show();)里面打开该模板；
            //$rootScope.actions.login(); 
            //console.log($rootScope.actions.login);  //=====================test
            
          }
          else {
            $state.go(toState);
          }
        }, function (err) {
          console.log('错误：Identification.checkToken()' + err);
          $ionicPopup.alert({title: '身份校验', template: '请检查网络！', okText: '确认'});
        });
      }
      
      //$state.transitionTo("login");
      event.preventDefault(); 
    }
  });
}])

.config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js

  // // 可以直接使用一个新的$stateProvider.state, 不需要abstract: true就可以自动加载到index.html里面唯一的<ion-nav-view></ion-nav-view>中;
  // // 不可以命名为init.Splashes, 这样表示父子state
  // $stateProvider.state('initSplashes', {
  //   url:'/initSplashes',
  //   access: { authenticate: false },
  //   templateUrl: 'templates/main/initSplashes.html',
  //   controller: 'initSplashes'
  // });

  $stateProvider

    // 不需要新建$stateProvider, 可以直接在同一个$stateProvider里面. 同理, 可以有多个abstract: true的父模板, 只要不指定views的名称(如: simple-container), 就加载到最层的<ion-nav-view>标签中
    .state('initSplashes', {
      url:'/initSplashes',
      access: { authenticate: false },
      templateUrl: 'templates/main/initSplashes.html',
      controller: 'initSplashes'
    })

    .state('simple', {
      url:'/simple',
      abstract: true,
      access: { authenticate: false },
      templateUrl: 'templates/simple.html',
      controller: 'simpleContainer'
    })

    // 首页
    .state('simple.homepage', {
      url:'/homepage',
      access: { authenticate: false },
      views: {
        'simple-container': {
          templateUrl: 'templates/main/homepage.html',
          controller: 'simpleHomepage'
        }
      }
    })

    // 我的主页
    .state('simple.mine', {
      url:'/mine',
      access: { authenticate: true },
      views: {
        'simple-container': {
          templateUrl: 'templates/main/mine.html',
          controller: 'simpleMine'
        }
      }
    })

    // 文章导航
    .state('simple.navArticle', {
      url:'/navArticle',
      access: { authenticate: false },
      views: {
        'simple-container': {
          templateUrl: 'templates/main/articleNav.html',
          controller: 'simpleNavArticle'
        }
      }
    })

    // 活动导航
    .state('simple.navActivity', {
      url:'/navActivity',
      access: { authenticate: false },
      views: {
        'simple-container': {
          templateUrl: 'templates/main/activityNav.html',
          controller: 'simpleNavActivity'
        }
      }
    })

    // 服务导航
    .state('simple.navService', {
      url:'/navService',
      access: { authenticate: false },
      views: {
        'simple-container': {
          templateUrl: 'templates/main/serviceNav.html',
          controller: 'simpleNavService'
        }
      }
    })

    // 文章列表
    .state('simple.articleList', {
      url:'/articleList/:tabCode',
      access: { authenticate: false },
      views: {
        'simple-container': {
          templateUrl: 'templates/article/articlelist.html',
          controller: 'simpleArticleList'
        }
      }
    })

    // 活动列表
    .state('simple.activityList', {
      url:'/activityList/:tabCode',
      access: { authenticate: true },
      views: {
        'simple-container': {
          templateUrl: 'templates/activity/activitylist.html',
          controller: 'simpleActivityList'
        }
      }
    })

    // 服务列表
    .state('simple.serviceList', {
      url:'/serviceList/:tabCode',
      access: { authenticate: false },
      views: {
        'simple-container': {
          templateUrl: 'templates/service/servicelist.html',
          controller: 'simpleServiceList'
        }
      }
    })

    // 文章页面
    .state('simple.article', {
      url:'/article/:articleId',
      access: { authenticate: false },
      views: {
        'simple-container': {
          templateUrl: 'templates/article/article.html',
          controller: 'simpleArticle'
        }
      }
    })

    // 活动页面
    .state('simple.activity', {
      url:'/activity/:activityId',
      access: { authenticate: true },
      views: {
        'simple-container': {
          templateUrl: 'templates/activity/activity.html',
          controller: 'simpleActivity'
        }
      }
    })

    // 心理1解1
    .state('simple.psychology', {
      url:'/psychology',
      access: { authenticate: false },
      views: {
        'simple-container': {
          templateUrl: 'templates/activity/psychology.html',
          controller: 'simplePsychology'
        }
      }
    })

    // 心理1解1链接
    .state('simple.psychologyLink', {
      url:'/psychologyLink/:qid',
      access: { authenticate: false },
      views: {
        'simple-container': {
          templateUrl: 'templates/activity/psychologyLink.html',
          controller: 'simplePsychologyLink'
        }
      }
    })

    // 搜索
    .state('simple.search', {
      url:'/search',
      access: { authenticate: false },
      views: {
        'simple-container': {
          templateUrl: 'templates/common/search.html',
          controller: 'simpleSearch'
        }
      }
    })

    // 发布
    .state('simple.publish', {
      url:'/publish',
      access: { authenticate: true },
      views: {
        'simple-container': {
          templateUrl: 'templates/person/publish.html',
          controller: 'simplePublish'
        }
      }
    })

    // 收藏
    .state('simple.favorites', {
      url:'/favorites',
      access: { authenticate: true },
      views: {
        'simple-container': {
          templateUrl: 'templates/main/favorites.html',
          controller: 'simpleFavorites'
        }
      }
    })

    // 设置
    .state('simple.settings', {
      url:'/settings',
      access: { authenticate: true },
      views: {
        'simple-container': {
          templateUrl: 'templates/main/settings.html',
          controller: 'simpleSettings'
        }
      }
    })

    // 关于我们
    .state('simple.ours', {
      url:'/ours',
      access: { authenticate: true },
      views: {
        'simple-container': {
          templateUrl: 'templates/settings/ours.html',
          controller: 'simpleOurs'
        }
      }
    })

    // 用户协议
    .state('simple.agreement', {
      url:'/agreement',
      access: { authenticate: true },
      views: {
        'simple-container': {
          templateUrl: 'templates/settings/agreement.html',
          controller: 'simpleAgreement'
        }
      }
    })

    // 意见反馈
    .state('simple.feedback', {
      url:'/feedback',
      access: { authenticate: true },
      views: {
        'simple-container': {
          templateUrl: 'templates/settings/feedback.html',
          controller: 'simpleFeedback'
        }
      }
    })

    // 个人资料
    .state('simple.personalInfo', {
      url:'/personalInfo',
      access: { authenticate: true },
      views: {
        'simple-container': {
          templateUrl: 'templates/person/information.html',
          controller: 'simplePersonalInfo'
        }
      }
    })

    // 签到管理
    .state('simple.personSignIn', {
      url:'/personSignIn',
      access: { authenticate: true },
      views: {
        'simple-container': {
          templateUrl: 'templates/person/signIn.html',
          controller: 'simplePersonSignIn'
        }
      }
    })

    // 个人留言
    .state('simple.personMessage', {
      url:'/personMessage',
      access: { authenticate: true },
      views: {
        'simple-container': {
          templateUrl: 'templates/person/message.html',
          controller: 'simplePersonMessage'
        }
      }
    })

    // 活动统计
    .state('simple.personManagement', {
      url:'/personManagement',
      access: { authenticate: true },
      views: {
        'simple-container': {
          templateUrl: 'templates/person/management.html',
          controller: 'simplePersonManagement'
        }
      }
    })

    // 活动统计详情
    .state('simple.personStatistics', {
      url:'/personStatistics',
      access: { authenticate: true },
      views: {
        'simple-container': {
          templateUrl: 'templates/person/statistics.html',
          controller: 'simplePersonStatistics'
        }
      }
    })

    // 活动提醒
    .state('simple.personWarn', {
      url:'/personWarn',
      access: { authenticate: true },
      views: {
        'simple-container': {
          templateUrl: 'templates/person/warn.html',
          controller: 'simplePersonWarn'
        }
      }
    })

    // 通讯录
    .state('simple.personAddressBook', {
      url:'/personAddressBook',
      access: { authenticate: true },
      views: {
        'simple-container': {
          templateUrl: 'templates/person/addressBook.html',
          controller: 'simplePersonAddressBook'
        }
      }
    })

    // 聊天室
    .state('simple.chatroom', {
      url:'/chatroom',
      access: { authenticate: true },
      views: {
        'simple-container': {
          templateUrl: 'templates/person/chatroom.html',
          controller: 'simpleChatroom'
        }
      }
    })
  ;

  // if none of the above states are matched, use this as the fallback
  // $urlRouterProvider.otherwise('/simple/homepage');
}])

.config(['$ionicConfigProvider', function ($ionicConfigProvider) {
  $ionicConfigProvider.prefetchTemplates(false);
}])

.config(['w5cValidatorProvider', function (w5cValidatorProvider) {

     // 全局配置
     w5cValidatorProvider.config({
         blurTrig   : false,
         showError  : true,
         removeError: true

     });
     w5cValidatorProvider.setRules({
         email: {
             //required : "输入的邮箱地址不能为空",
             email    : "输入邮箱的格式不正确"
         },
         username: {
             required : "输入的用户名不能为空",
             pattern  : "用户名必须输入字母、数字、下划线,以字母开头"
         },
         password: {
             required : "密码不能为空",
             minlength: "密码长度不能小于{minlength}",
             maxlength: "密码长度不能大于{maxlength}"
         },
         repeat_password: {
                repeat: "两次填写的密码不一致"
         },
         chinese_name : {
             required : "姓名不能为空",
             pattern  : "请正确输入中文姓名"
         },
         mobile: {
             required : "手机号不能为空",
             pattern  : "请填写正确手机号",
             minlength: "手机号长度不能小于{minlength}",
             maxlength: "手机号长度不能大于{maxlength}"
         }
     });
 }]);


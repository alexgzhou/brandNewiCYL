angular.module('icyl.directives', [])

// 输入框清除按钮
.directive("buttonClearInput", ['$compile', function ($compile) {
    return {
        restrict: "AE",
        scope: {
            input: "="  //这里可以直接用input获取父scope(包括兄弟元素)中ng-model的值, 传递给本directive创建的isolate scope使用, template也属于当前isolate scope
        },
        // replace: true,   //使用replace之后, 本元素的click不能删除输入框中的内容, 原因大致可以理解为: 父元素被替换后, scope.$apply没有执行对象
        template:"<button ng-if='input' class='button button-icon ion-android-close input-button padding-right' type='button' ng-click='clearInput()'></button>",
        controller: function ($scope, $element, $attrs) {
            // console.log($scope.input);
            $scope.clearInput = function () {
                // scope.item.searchContent = '';
                console.log($scope.input);
                $scope.input = "";
            };
            // $compile($element.contents())($scope.$new());
        }
        // link: function (scope, element, attrs) {
        //     // console.log(scope.$id);
        //     // console.log(scope.input);
        //     // scope.input = "";

        //     var onclick = function () {
        //         console.log(scope.input);
        //         scope.$apply(function () {
        //             // scope.item.searchContent = '';
        //             // console.log(scope.input);
        //             scope.input = "";
        //         });
        //     };

        //     element.bind("click", onclick);
        //     console.log(element);
        //     scope.$on("$destroy", function (e) {
        //         element.unbind("click", onclick);
        //         // console.log("unbind on destory: " + e);
        //     });
        // }
    };
}])

.directive('imgCache', ['$document', function ($document) {
    return {
        restrict: "A",
        // require: 'ngSrc',
        // scope: {
        //     // ngSrc: "=", // 监控这个属性会出错，是因为有{{expression}}这样的形式，以及&&等运算符，如果仅有vara+varb这样的运算，是可以的
        //     // imgBase: "=",
        //     // imgAlter: "=",
        //     imgCache: "="
        // },
        link: function (scope, elem, attrs) {
            // var target = elem;
            // // var target = angular.element(elem);
            // // console.log(scope.imgCache);
            // // console.log(target);
            // // console.log(ImgCache.ready+'=====================');

            // var watchImgCallback = function (newValue) {
            //     // alert(newValue);
            //     if (newValue) {
            //         newValue = scope.imgBase + scope.imgCache;
            //         // console.log(newValue);
            //         ImgCache.isCached(newValue, function (path, success) {
            //             if (success) {
            //                 // already cached
            //                 ImgCache.useCachedFile(target);
            //             } 
            //             else {
            //                 // not there, need to cache the image
            //                 ImgCache.cacheFile(newValue, function(){
            //                     ImgCache.useCachedFile(target);
            //                 });
            //                 // console.log(newValue);
            //             }
            //         });
            //     }
            //     else {
            //         newValue = scope.imgAlter;
            //         // console.log(newValue);
            //         ImgCache.isCached(newValue, function (path, success) {
            //             if (success) {
            //                 // already cached
            //                 ImgCache.useCachedFile(target);
            //             } 
            //             else {
            //                 // not there, need to cache the image
            //                 ImgCache.cacheFile(newValue, function(){
            //                     ImgCache.useCachedFile(target);
            //                 });
            //                 // console.log(newValue);
            //             }
            //         });
            //     }
            // };
            
            // var imgLoad = imagesLoaded(elem);
            var ngSrcObservCallback = function(src) {
                // console.log(scope.imgCache);
                // imagesLoaded(elem, function (instance) {
                //     console.log(instance.hasAnyBroken);
                // });
                ImgCache.isCached(src, function(path, success) {
                    if (success) {
                        ImgCache.useCachedFile(elem);
                    } else {
                        // var loadImg = function () {
                            imagesLoaded(elem, function (instance) {
                                if (!instance.hasAnyBroken) {
                                    ImgCache.cacheFile(src, function() {
                                        ImgCache.useCachedFile(elem);
                                    });
                                }
                                // else {
                                //     loadImg();
                                // }
                            });
                        // };
                        // loadImg();
                        // console.log(loadImg);
                    }
                });
            }

            //Waits for the event to be triggered (only once),
            //before executing callback;
            //or executing callback when ImgCache.ready
            // if (ImgCache.ready) {
                // scope.$watch('imgCache', watchImgCallback);  //用scope.$watch保险一点，应该可以与collection-repeat联合使用，但是没有成功？
                // watchImgCallback(scope.imgCache);   //万一网络出现问题，scope.imgCache未定义，会造成错误，优点是性能好一点
                attrs.$observe('ngSrc', ngSrcObservCallback);
                // attrs.$observe('src', ngSrcObservCallback);
            // }
            // else {
                var release = scope.$on('ImgCacheReady', function () {
                    //this checks if we have a cached copy.
                    // console.log(attrs.ngSrc);
                    // scope.$watch('imgCache', watchImgCallback);  //用scope.$watch保险一点，应该可以与collection-repeat联合使用，但是没有成功？
                    // watchImgCallback(scope.imgCache);   //万一网络出现问题，scope.imgCache未定义，会造成错误，优点是性能好一点
                    // attrs.$observe('ngSrc', ngSrcObservCallback);
                    attrs.$observe('src', ngSrcObservCallback);
                }, false); 

                scope.$on("$destroy", function (e) {
                    release();
                });
            // }
        }
    };
}])

// 信息发布功能
.directive("publishInfo", ['$ionicActionSheet', '$location', function ($ionicActionSheet, $location) {
    return {
        restrict: "A",
        scope: {
            publishInfo: "="
        },
        link: function (scope, element, attrs) {
            // console.log(scope.$id);
            // console.log(attrs.class);
            // console.log(scope.publishInfo);
            var onclick = function () {
                // Triggered on a button click, or some other target
                scope.$apply(function () {  //不加scope.$apply会导致$ionicActionSheet.show出现卡顿
                    var hideSheet = $ionicActionSheet.show({
                        buttons: [
                            { text: '发布文章' },
                            { text: '发布活动' }
                        ],
                        titleText: '发布',
                        cancelText: '取消',
                        cancel: function() {
                            // add cancel code..
                        },
                        buttonClicked: function(index) {
                            if(index === 0){
                                $location.path('/simple/publish');
                            }
                            return true;
                        }
                    });
                });
            };
            
            element.bind( "click", onclick);
            scope.$on("$destroy", function (e) {
                element.unbind("click", onclick);
                // console.log("unbind on destory");
            });
        }
    };
}])

/*
// 自定义iframe，和default.html:114配合使用：done
.directive("iframeSetCookie", ['$window', 'Storage', function ($window, Storage) {
    return {
        restrict: "E",
        replace: true,
        //template:"<div style='display:none'></div>",
        link: function (scope, element, attrs) {
            //var iframeId = attrs.id;
            var iframeSrc = attrs.src;
            var iframeStyle = attrs.style;

            //console.log(element);    //====================test

            var iframe = $window.document.createElement('iframe');
            //iframe.setAttribute('src', iframeSrc);
            iframe.src = iframeSrc;
            //iframe.id = iframeId;
            iframe.style = iframeStyle;
            element[0].appendChild(iframe);
            //console.log(iframe);    //====================test
            // console.log(iframe.contentWindow);    //====================test
            //console.log('iframeSetCookie'); //=============test

            cookie = !!Storage.kget('xsunion') ? Storage.kget('xsunion') : false;
            //cookie = 'xsunion=staff%5Fsts=2&telephone=0571%2D83731771&card5=900000001&name=900006840&dw=%B3%F8%C1%F4%CF%E3%B4%A8%B2%CB%BB%F0%B9%F8&card4=900000002&card2=900006840&card%5Fno1=900006840&shopid1=900000003&staff%5Fgrade=1&reg%5Fnbr=900006840&card3=900000003'; //=============test

            function handMessage(event){
                event = event || $window.event;
                //验证是否来自预期内的域，如果不是不做处理，这样也是为了安全方面考虑
                // if(iframeSrc.indexOf(event.origin)>-1){
                //     if (iframe.contentWindow && !!cookie && event.data=="ready") {
                //         iframe.contentWindow.postMessage(cookie, event.origin);
                //         //console.log(event.data); //=============test
                //     }
                //     else {
                //         //console.log(event.data); //=============test
                //         if (!!event.data && event.data.indexOf('xsunion')>-1 && event.data.length>60) {
                //             Storage.kset('xsunion', event.data);
                //         }
                //         else {
                //             Storage.kremove('xsunion');
                //         }
                //     }
                // }
                if(iframe.contentWindow && !!event.data && iframeSrc.indexOf(event.origin)>-1){
                    if (event.data=="ready") {
                        if (!!cookie) {
                            iframe.contentWindow.postMessage(cookie, event.origin);
                            // console.log(event.origin); //=============test
                        }
                    }
                    if (event.data.indexOf('xsunion')>-1) {
                        if (event.data.length>60) {
                            Storage.kset('xsunion', event.data);
                        }
                        else {
                            Storage.kremove('xsunion');
                        }
                    }
                    if (event.data.indexOf('http://')===0) {
                        //var socialSharing = event.data.split("<$separate$>");
                        Storage.kset('socialSharing', event.data);
                    }
                }
            }

            // function storageEvent(event) {
            //     console.log('event');
            // }

            //给window对象绑定message事件处理
            if($window.addEventListener){
                $window.addEventListener("message", handMessage, false);
                //Some browsers don't support the storage event, and most of the browsers that do support it will only call it when the storage is changed by a different window. So, open your page up in two windows. Click the links in one window and you will probably see the event in the other.    The assumption is that your page will already know all interactions with localStorage in its own window and only needs notification when a different window changes things. This, of course, is a foolish assumption. But, localStorage is a new thing. Hopefully, they'll eventually figure it all out.
                // $window.addEventListener("storage", storageEvent, true);
                // console.log('addEventListener: iframeSetCookie');
            }
            else{
                $window.attachEvent("onmessage", handMessage);
                // $window.attachEvent("onstorage", storageEvent);
            }
        }
    };
}])
*/

// 自定义分享按钮(每个页面只能有一个本元素)
.directive("socialSharing", ['Storage', function (Storage) {
    return {
        restrict: "A",
        link: function (scope, element, attrs) {
            Storage.kremove('socialSharing');
            var onclick = function () {
                if (!!Storage.kget('socialSharing')) {
                    var sharing = Storage.kget('socialSharing').split("<$separate$>");
                    //window.alert(sharing);    //===============test
                    console.log(element[0]);    //===============test
                    console.log(sharing);    //===============test
                    document.getElementById('test').innerHTML = sharing;    //===============test
                    window.plugins.socialsharing.share(
                        !!sharing[2] ? sharing[2] : '这个平台不错的！',   //'信息、主题图片和链接', 
                        null,   //'智慧团青:',   //!!sharing[1] ? sharing[1] : '请关注这个平台！',   //'主题', 
                        null,   //!!sharing[3] ? sharing[3] : null,     //'图片地址',
                        !!sharing[0] ? sharing[0] : 'http://17f.go5le.net/bootstrap-3.1.1/',    //'网址',
                        function (result) {
                            console.log('result: ' + result);
                            //window.alert('result: ' + result);    //===============test
                            //Storage.kremove('socialSharing');
                            scope.$apply(function () {
                                //element[0].value = sharing[0]+sharing[2]+sharing[3]+result;
                                //element[0].innerText = sharing[0]+sharing[2]+sharing[3]+result;
                                document.getElementById('test').innerHTML = sharing[0]+sharing[2]+sharing[3]+result;
                            });
                        },
                        function (error) {
                            window.alert('error: ' + result);
                            //Storage.kremove('socialSharing');
                        }
                    );
                }
            };

            element.bind( "click", onclick);
            scope.$on("$destroy", function (e) {
                element.unbind("click", onclick);
                // console.log("unbind on destory");
            });
        }
    };
}])

// 打开外部页面按钮：封装了Cordova插件inAppBrowser
.directive("openExternal", ['$windos', function ($window) {
    return{
        restrict: 'E',
        scope: {
            url : "=",
            cookie: "=",
            exit : "&",
            loadOpen : "&",
            loadStop : "&",
            loadError: "&"
        },
        replace: true,
        transclude: true,
        template:"<a class='button center-block background-clear' ng-click='openUrl()'><span ng-transclude></span></a>",
        // link: function( scope, element, attrs ) {
        //     console.log(scope.$id);    //====================test
        // },
        controller: function ($scope) {

            // var wrappedFunction = function(action){
            //     return function(){
            //         $scope.$apply(function(){
            //             action();
            //         });
            //     }
            // };   //没必要用apply，除非需要在主界面上同步显示这一$scope中的变量变化

            var inAppBrowserClosed = function () {
                if(inAppBrowser !== null){
                    //console.log("did it");    //====================test
                    //inAppBrowser.removeEventListener('exit', wrappedFunction($scope.exit));
                    if($scope.loadOpen){
                        inAppBrowser.removeEventListener('loadstart', inAppBrowserStart);
                        //$window.alert('removeEventListener: $scope.loadStart');    //====================test
                    }
                    if($scope.loadStop){
                        inAppBrowser.removeEventListener('loadstop', inAppBrowserStop);
                        //$window.alert('removeEventListener: $scope.loadStop');    //====================test
                    }
                    if($scope.loadError){
                        inAppBrowser.removeEventListener('loaderror', $scope.loadError);
                        //$window.alert('removeEventListener: $scope.loadError');    //====================test
                    }
                    if($scope.exit){
                        inAppBrowser.removeEventListener('exit', inAppBrowserClosed);
                        //$window.alert('removeEventListener: $scope.exit');    //====================test
                    }
                    $scope.exit();
                }
            };
            var inAppBrowserStart = function () {
                if(inAppBrowser !== null){
                    if($scope.loadOpen){
                        //$window.alert($scope.url);
                        inAppBrowser.executeScript({
                                code: 'document.cookie="' + $scope.cookie + '";'
                            },
                            function (values) {
                                //$window.alert('document.cookie======'+values[0]);
                            }
                        );
                        //inAppBrowser.removeEventListener('loadstart', inAppBrowserStart);
                        $scope.loadOpen();
                    }
                }
            };
            var inAppBrowserStop = function () {
                if(inAppBrowser !== null){
                    if($scope.loadStop){
                        //$window.alert($scope.url);
                        inAppBrowser.executeScript({
                                code: 'document.cookie.match(new RegExp("(^| )xsunion=([^;]*)(;|$)"));'
                            },
                            function (values) {
                                //$window.alert('document.cookie.match======'+values[0][0]);
                            }
                        );
                        //inAppBrowser.removeEventListener('loadstop', inAppBrowserStop);
                        $scope.loadStop();
                    }
                }
            };
            //console.log("did it");    //====================test
            //$scope.exit(); //====================test
            //console.log($scope.$id);    //====================test
            var inAppBrowser;// = $window.open('http://17f.go5le.net/preload.html','_blank','hidden=yes');
            $scope.openUrl = function () {
                inAppBrowser = $window.open(encodeURI($scope.url),'_blank','location=yes');
                // inAppBrowser.executeScript({
                //     code: "document.cookie = 'xsunion=staff%5Fsts=2&telephone=0571%2D83731771&card5=900000001&name=900006840&dw=%B3%F8%C1%F4%CF%E3%B4%A8%B2%CB%BB%F0%B9%F8&card4=900000002&card2=900006840&card%5Fno1=900006840&shopid1=900000003&staff%5Fgrade=1&reg%5Fnbr=900006840&card3=900000003'"
                //     },
                //     function(values) {
                //         $window.alert(values.toString());
                //     }
                // );
                // inAppBrowser.executeScript({code: 'document.cookie="hello_world";alert(document.cookie)'},
                //     function(){
                //         $window.alert('executeScript');
                //     }
                // );
                //console.log("did it");    //====================test
                if($scope.loadOpen instanceof Function){
                    //inAppBrowser.addEventListener('loadstart', wrappedFunction($scope.loadOpen));
                    //inAppBrowser.addEventListener('loadstart', function(event){ alert(event.type+': '+event.url); });
                    inAppBrowser.addEventListener('loadstart', inAppBrowserStart);
                    //console.log($scope.loadStart);    //====================test
                }
                if($scope.loadStop instanceof Function){
                    inAppBrowser.addEventListener('loadstop', inAppBrowserStop);
                    //console.log($scope.loadStop);    //====================test
                }
                if($scope.loadError instanceof Function){
                    inAppBrowser.addEventListener('loaderror', $scope.loadError);
                    //console.log($scope.loadError);    //====================test
                }
                if($scope.exit instanceof Function){
                    inAppBrowser.addEventListener('exit', inAppBrowserClosed);
                    //console.log($scope.exit);    //====================test
                    //$scope.exit(); //====================test
                    //var action = wrappedFunction($scope.exit);  //====================test
                    //console.log(action);    //====================test
                    // var wrapped = function(action){
                    //                 return action();
                    //               };
                    // wrapped($scope.exit);
                    // console.log(wrapped($scope.exit));    //====================test
                    //action();   //====================test
                    //console.log($scope.$id);    //====================test
                }
                //console.log(inAppBrowser);    //====================test
            };

            // $scope.$on("$destroy", function(){
            //     console.log(inAppBrowser);    //====================test
            //     if(inAppBrowser != null){
            //         //console.log("did it");    //====================test
            //         inAppBrowser.removeEventListener('exit', wrappedFunction($scope.exit));

            //         if($scope.exit){
            //             inAppBrowser.removeEventListener('exit', wrappedFunction($scope.exit));
            //         }
            //         if($scope.loadStart){
            //             inAppBrowser.removeEventListener('loadstart', wrappedFunction($scope.loadStart));
            //         }
            //         if($scope.loadStop){
            //             inAppBrowser.removeEventListener('loadstop', wrappedFunction($scope.loadStop));
            //         }
            //         if($scope.loadError){
            //             inAppBrowser.removeEventListener('loaderror', wrappedFunction($scope.loadError));
            //         }
            //     }

            // });
        }
    };
}])
;